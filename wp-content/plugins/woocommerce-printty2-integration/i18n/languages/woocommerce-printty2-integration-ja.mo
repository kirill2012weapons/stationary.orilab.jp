��    8      �  O   �      �     �     �     �                  
   -     8     G     T     d     s  Q   �     �     �  `   �  
   W     b     o     x     �      �     �  !   �          )     ;     M     ]     k     w     �     �     �     �     �  h   �     I     c  a   t     �  l   �  |   \	     �	     �	     �	      	
     *
  Z   /
     �
     �
     �
  	   �
     �
  8   �
  �  �
     �     �     �     �     �     �  
                   6     F     Y  Y   `     �     �  i   �     G     `     |     �     �     �     �     �     �                     $     +     4     D      Q     r     �     �  h   �            i   .     �  E   �  ?   �     +     >     W     j     �  _   �     �     �       	          8   !     8                                             4   +      .         ,   &          (   /                  %          #                           6   '            "   0   2       	      !      3            1          )                                     
   7   $   5   *       -       12:00-16:00 16:00-20:00 3 business day 4 business day 5 business day 7 business day 8:00-12:00 9 business day Consultation Data Submission Delete Invoice Delivered Order Delivered<span class="count">(%s)</span> Delivered<span class="count">(%s)</span> Delivery Service Delivery Source Failed(Printty2) <span class="count">(%s)</span> Failed(Printty2)<span class="count">(%s)</span> FetchOrder Image Status MailTest Order statusAwaiting data Order statusDelivered Order statusDelivered(Printty2) Order statusFailed(Printty2) Order statusProcessing(Printty2) Order statusSended(Printty2) Order to Printty2 Ordered Printty2  Printty2 Amount Printty2 Date Printty2 ID Printty2 Image Url Printty2 Number Printty2 Preview Url Printty2 Situation Printty2 Status Priority Type Processing(Printty2)<span class="count">(%s)</span> Processing(Printty2) <span class="count">(%s)</span> Production Date Preferred Register Invoice Sended(Printtry2)<span class="count">(%s)</span> Sended(Printtry2)<span class="count">(%s)</span> Shipping Delivery Period This is an order notification sent to customers containing order details after an order is placed delivered. This is an order notification sent to customers containing order details after an order is placed processing order printty2. Tracking Number Update Order Status Use Package Woocommerce Printty2 Integration auto awaiting data <span class="count">(%s)</span> awaiting data<span class="count">(%s)</span> http://spicelife.jp/ none not use spicelife use wordpressとprintty2の連携機能を提供します。 Project-Id-Version: 0.1
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/woocommerce-printty2-integration
POT-Creation-Date: 2018-02-04 14:41:32+00:00
PO-Revision-Date: 2017-09-24 13:51+0000
Last-Translator: Matsunaga Kenichi <kenichi.matsunaga@spicelife.jp>
Language-Team: SpiceLife <kenichi.matsunaga@spicelife.jp>
Language: Japanese
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 12時~16時 16時~20時 3営業日以内 4営業日以内 5営業日以内 7営業日以内 8時~12時 9営業日以内 デザインを相談 データ入稿 請求書の削除 発送 配送済み <span class="count">(%s)</span> 配送済み <span class="count">(%s)</span> 発送サービス 発送元情報 発注失敗(丸井) <span class="count">(%s)</span> 発注失敗(丸井) <span class="count">(%s)</span> オーダ情報の取得 画像取込ステータス メールテスト データ待ち 配送済み 発送済み(丸井) 発注失敗(丸井) 発注済み(丸井) 発送済み(丸井) 発注 丸井さんに発注 金額 日付 注文ID 発注用画像 注文Number 発注用画像(プレビュー) 丸井発注状況 ステータス 出荷希望 発注済み(丸井)<span class="count">(%s)</span> 発注済み(丸井) <span class="count">(%s)</span> 製造予定日 請求書を登録 発送済み(丸井) <span class="count">(%s)</span> 発送済み(丸井) <span class="count">(%s)</span> 配送時間 注文が発送済みとなった時にメール配信されます。 丸井さん発注時にユーザにメール配信します。 配送伝票番号 ステータスの更新 ギフトセット Printty2 連携機能 自動 データ待ち <span class="count">(%s)</span> データ待ち <span class="count">(%s)</span> http://spicelife.jp/ なし 使わない spicelife 使う wordpressとprintty2の連携機能を提供します。 