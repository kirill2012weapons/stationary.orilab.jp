<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Transactional Emails Controller
 *
 * WooCommerce Emails Class which handles the sending on transactional emails and email templates. This class loads in available emails.
 *
 * @class 		WCP_Emails
 * @version		2.3.0
 * @package		WooCommerce/Classes/Emails
 * @category	Class
 * @author 		WooThemes
 */
class WCP_Emails
{
	function __construct(){
		add_filter( 'woocommerce_email_classes', array( $this, 'filter_woocommerce_email_classes' ));
		add_filter( 'woocommerce_email_actions', array( $this, 'filter_woocommerce_email_actions' ));

	}

	public function filter_woocommerce_email_classes( $emails ) {
		$emails['WCP_Email_Customer_Processing_Order_Printty2'] = include( 'emails/class-wcp-email-customer-processing-order-pritty2.php' );
		$emails['WCP_Email_Customer_Delivered_Order'] = include( 'emails/class-wcp-email-customer-delivered-order.php' );
		$emails['WCP_Email_Customer_Request_Customers_Voice'] = include( 'emails/class-wcp-email-customer-request-customers-voice.php' );

		return $emails;
	}

	public function filter_woocommerce_email_actions( $actions ) {
		$actions[] = 'customer_processing_order_printty2';
		$actions[] = 'customer_deliverd_order';
		$actions[] = 'customer_request_customers_voice';

		return $actions;
	}
}
new WCP_Emails();
