<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package storefront
 */

get_header(); ?>
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

    <!-- 新着記事 -->
    <?php if ( !is_paged() ) : // 1ページ目 ?>
      <?php
       $list = get_posts( array(
        'category_name' => 'magazine',
        'posts_per_page' => 2
       ));
      if( $list ):
      ?>
      <div class="new-arrivals">
        <?php
           foreach( $list as $post ):
           setup_postdata( $post );
        ?>
          <div class="new-arrival">
            <div class="thumbnail">
              <a href="<?php the_permalink(); ?>"><?php 
                if ( has_post_thumbnail() ) { // アイキャッチ画像が割り当てられているか
                  the_post_thumbnail();
              } ?></a>
            </div>
            <div class="description">
              <p><a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a></p>
              <p class="time"><?php the_time('Y年m月d日'); ?></p>
            </div>
          </div><!-- .new-arrival -->
          <?php
            endforeach;
            wp_reset_postdata();
          ?>
        </div><!-- .new-arrivals -->
      <?php endif; ?>
    <?php endif; ?>

    <!-- 記事リスト -->
    <ul class="magazine-list">
      <?php if(have_posts()): while(have_posts()): the_post(); $counter++; ?>
        <?php if ($counter <= 2): ?>
        <?php else:?>
          <li>
          	<div class="magazine-thumbnail">
          		<a href="<?php the_permalink(); ?>"><?php 
          			if ( has_post_thumbnail() ) { // 投稿にアイキャッチ画像が割り当てられているか
          				the_post_thumbnail();
          		} ?></a>
          	</div>
          	<div class="magazine-description">
          		<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
          	  <p class="time"><?php the_time('Y年m月d日'); ?></p>
            </div>
          </li>
        <?php endif;?>
      <?php endwhile; endif; ?>
    </ul>
    <div class="navigation">
      <?php page_navi(); ?>
    </div>	
  </main><!-- #main -->
</div><!-- #primary -->
<?php
  do_action( 'storefront_sidebar' );
  get_footer();
?>
