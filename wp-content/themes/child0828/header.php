<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>


    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NBKVCWS');</script>
    <!-- End Google Tag Manager -->



    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0", user-scalable=no>
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://original-smaphocase.com/wp-admin/images/">
    <meta property="og:type" content="artcle" />

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); echo '?' . filemtime( get_stylesheet_directory() . '/style.css'); ?>" type="text/css" />

    <?php wp_enqueue_script('jquery'); ?>
    <?php wp_head(); ?>


    <!-- サイドメニュー（PC用） -->
    <script type="text/javascript">
        jQuery(function ($) {
            var url = new URL(window.location.href);
            var logout = url.searchParams.get("logout");
            if (logout === 'true') {
                localStorage.clear();
            }

            if (localStorage.getItem("UserLogin") != null) {
                $("#manager_div").show();
                $("#cart_img").hide();
                $("#account_img").hide();
                $("#logout_img").show();
                $("#searchAndCart").hide();

                $(".user-name").text(localStorage.getItem("UserNickName"));
                var currentHost = window.location.hostname;

                $("#append-session li").each(function (index) {
                    if ($(this).find('a')) {
                        $(this).find('a').attr('href', $(this).find('a').attr('href') + '&session=' + localStorage.getItem("UserLogin")+'&host='+currentHost);
                    }
                })

                $("#logout_btn").attr('href','');
            } else {
                $("#manager_div").hide();
                $("#cart_img").show();
                $("#logout_img").hide();
                $("#searchAndCart").hide();
            }

            $('.hover-menu').hover(function () {
                $(this).children('.sub-menu').show();
            }, function () {
                $(this).children('.sub-menu').hide();
            });

            $("#logout_btn, #logout_img").on('click', function () {

                $.post('https://api.orilab.jp/signout', {
                    "session": localStorage.getItem("UserLogin")
                }, function(serverResponse){

                })

                localStorage.clear();
                window.location.href = window.location.origin

                return false
            });


        });
    </script>


    <!-- トグルメニュー（SP用） -->
    <script type="text/javascript">
        jQuery(function($){
            $('#menu-mobile-toggle-menu>.menu-item>a').click(function() {
                $(this).next('.sub-menu').slideToggle(1000);
            });
        });
    </script>

    <!-- プレースホルダーの改変 -->
    <script type="text/javascript">
        jQuery(function($){
            $('input#billing_last_name').attr("placeholder","オリラボ");
            $('input#billing_first_name').attr("placeholder","太郎");
            $('input#billing_phone').attr("placeholder","000-0000-0000");
            $('input#billing_email').attr("placeholder","orilab@olilab.jp");
        });
    </script>

    <meta name="google-site-verification" content="2scO5KfRni0ymCq38AvFZTn0PaFpS0YEQsHft_9-mos" />
</head>

<body <?php body_class(); ?>>

<script type="text/javascript">
    window._mfq = window._mfq || [];
    (function() {
        var mf = document.createElement("script");
        mf.type = "text/javascript"; mf.async = true;
        mf.src = "//cdn.mouseflow.com/projects/be114b07-f575-4033-b05e-b4c23d12d2e8.js";
        document.getElementsByTagName("head")[0].appendChild(mf);
    })();
</script>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NBKVCWS"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<?php do_action( 'storefront_before_site' ); ?>
<div id="page" class="hfeed site">
    <?php do_action( 'storefront_before_header' ); ?>

    <header id="masthead" class="site-header" role="banner" style="<?php storefront_header_styles(); ?>">
        <div class="col-full">
            <?php
            /**
             * Functions hooked into storefront_header action
             *
             * @hooked storefront_skip_links                       - 0
             * @hooked storefront_social_icons                     - 10
             * @hooked storefront_site_branding                    - 20
             * @hooked storefront_secondary_navigation             - 30
             * @hooked storefront_product_search                   - 40
             * @hooked storefront_primary_navigation_wrapper       - 42
             * @hooked storefront_primary_navigation               - 50
             * @hooked storefront_header_cart                      - 60
             * @hooked storefront_primary_navigation_wrapper_close - 68
             */
            ?>
        </div>

        <!-- ヘッダー上部 -->

        <?php if (!is_mobile()): ?>
            <!-- PCの場合 -->
            <div class="col-full">

                <div class="original-header">

                    <div class="original-header-left">
                        <?php
                        if ( function_exists( 'the_custom_logo' ) ) {
                            the_custom_logo();
                        } ?>
                        <img src="/wp-content/uploads/2017/09/edf0da09b194bc86b1916311186613f6.png" alt="お客様満足度92%" class="header-icon">
                        <img src="/wp-content/uploads/2017/09/0b8bd6373f810f71208b0e716066ac15.png" alt="制作実績30万件以上" class="header-icon">

                    </div>

                    <div class="original-header-right">

                        <div id="manager_div" class="user-wrap">
                            <div class="holder">
                                <ul id="append-session">

                                    <li><strong class="user-name"></strong></li>
                                    <li><a href="https://drawer.orilab.jp/index.html?url_site=http://sp-beta.orilab.jp/product/mobile-battery-indicator-none-both-sides-4000mah&model_id=1433&login_type=user"><span>新規デザイン作成</span></a></li>
                                    <li><a href="<?php echo UP_T_URL; ?>/search.php?type=pay"><span>注文履歴</span></a></li>
                                    <li><a href="<?php echo UP_T_URL; ?>/search.php?type=user_fee"><span>報酬支払明細</span></a></li>
                                    <li><a href="<?php echo UP_T_URL; ?>/view.php?type=report"><span>売上レポート</span></a></li>
                                    <li><a href="<?php echo UP_T_URL; ?>/search.php?type=item&design=my"><span>MYデザイン</span></a></li>
                                    <li><a href="<?php echo UP_T_URL; ?>/search.php?type=item&design=owner"><span>二次利用作品一覧</span></a></li>
                                    <li><a href="<?php echo UP_T_URL; ?>/edit.php?type=user"><span>アカウント設定</span></a></li>
                                    <li><a href="<?php echo UP_T_URL; ?>/edit.php?type=user&design=pass"><span>パスワード変更</span></a></li>
                                    <li><a href="<?php echo UP_T_URL; ?>/search.php?type=item_favorite"><span>お気に入り</span></a></li>
                                    <li><a id="logout_btn" href="">ログアウト</a></li>
                                    <li><a href="<?php echo UP_T_URL; ?>/edit.php?type=user&design=delete"><span>退会</span></a></li>
                                </ul>
                            </div>
                        </div>

                        <a href="/info/faq" class="faq_btn"><img src="https://original-smaphocase.com/wp-content/uploads/2017/12/head-btn-faq.png""></a>
                        <a href="/info/order" class="soudan_btn"><img src="https://original-smaphocase.com/wp-content/uploads/2017/12/head-btn-soudan.png"></a>
                        <a href="/contact">
                            <img src="https://original-smaphocase.com/wp-content/uploads/2017/12/head_contact_btn.png" alt="お問い合わせ" class="header-icon">
                        </a>
                        <a id="cart_img" href="/cart">
                            <img src="/wp-content/uploads/2017/12/ico-cart.png"　alt="カート" class="header-icon">
                        </a>
                        <a id="account_img" href="/account">
                            <img src="/wp-content/uploads/2017/09/086f440e8ff500aaa2b90dcb16b274fb.png"　alt="ログイン" class="header-icon">
                        </a>

                        <a id="logout_img">
                            <img style="background:#ff0101; cursor: pointer;" src="/wp-content/themes/child0828/images/btn-logout.png"　alt="ログアウト" class="header-icon">
                        </a>

                    </div>
                </div><!-- original-header -->
            </div>
            <div class="original-gnavi"><div class="col-full">
                    <nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_html_e( 'Primary Navigation', 'storefront' ); ?>">
                        <button class="menu-toggle" aria-controls="site-navigation" aria-expanded="false"><span><span class="menu-title"><?php echo esc_attr( apply_filters( 'storefront_menu_toggle_text', __( 'Menu', 'storefront' ) ) ); ?></span></span></button>
                        <?php
                        wp_nav_menu(
                            array(
                                'theme_location'	=> 'primary',
                                'container_class'	=> 'primary-navigation',
                            )
                        );

                        wp_nav_menu(
                            array(
                                'theme_location'	=> 'handheld',
                                'container_class'	=> 'handheld-navigation',
                            )
                        );
                        ?>
                    </nav>
                </div></div>
        <?php else: ?>
            <!-- SPの場合 -->
            <div class="original-header original-header-sp">
                <div class="responsive-menu-area"></div>

                <div class="original-header-left-sp">
                    <a href="https://original-smaphocase.com/" class="custom-logo-link" rel="home" itemprop="url">
                        <img src="/wp-content/uploads/2018/06/nobori_2C.png" class="custom-logo" alt="オリジナルスマホケース作成のオリラボ" itemprop="logo" srcset="/wp-content/uploads/2018/06/nobori_2C.png 329w, /wp-content/uploads/2018/06/cropped-nobori_2C.png 300w" sizes="(max-width: 329px) 100vw, 329px"></a>
                </div>

                <div class="button-area" style="margin-left:auto;">
<!--                    <ul id="searchAndCart">-->
<!--                        <li class="search">-->
<!--                            <a href="javascript:void(0)" onClick="jQuery('.storefront-handheld-footer-bar .search > a').trigger('click');">検索</a>-->
<!--                        </li>-->
<!--                        <li class="cart">-->
<!--                            <a class="footer-cart-contents" href="" title="ショッピングカートを見る" ></a>-->
<!--                        </li>-->
<!--                    </ul>-->
                    <a id="account_img" href="/account">
                        <img style="position: absolute; top: 0; right: 1px; height: 50px;" src="/wp-content/uploads/2017/09/086f440e8ff500aaa2b90dcb16b274fb.png"　alt="ログイン" class="header-icon">
                    </a>
                    <div id="manager_div" class="user-wrap-smart">
                        <div class="holder">
                            <ul id="append-session">

                                <li><strong class="user-name"></strong></li>
                                <li><a href="https://drawer.orilab.jp/index.html?url_site=http://sp-beta.orilab.jp/product/mobile-battery-indicator-none-both-sides-4000mah&model_id=1433&login_type=user"><span>新規デザイン作成</span></a></li>
                                <li><a href="<?php echo UP_T_URL; ?>/search.php?type=pay"><span>注文履歴</span></a></li>
                                <li><a href="<?php echo UP_T_URL; ?>/search.php?type=user_fee"><span>報酬支払明細</span></a></li>
                                <li><a href="<?php echo UP_T_URL; ?>/view.php?type=report"><span>売上レポート</span></a></li>
                                <li><a href="<?php echo UP_T_URL; ?>/search.php?type=item&design=my"><span>MYデザイン</span></a></li>
                                <li><a href="<?php echo UP_T_URL; ?>/search.php?type=item&design=owner"><span>二次利用作品一覧</span></a></li>
                                <li><a href="<?php echo UP_T_URL; ?>/edit.php?type=user"><span>アカウント設定</span></a></li>
                                <li><a href="<?php echo UP_T_URL; ?>/edit.php?type=user&design=pass"><span>パスワード変更</span></a></li>
                                <li><a href="<?php echo UP_T_URL; ?>/search.php?type=item_favorite"><span>お気に入り</span></a></li>
                                <li><a id="logout_btn" href="">ログアウト</a></li>
                                <li><a href="<?php echo UP_T_URL; ?>/edit.php?type=user&design=delete"><span>退会</span></a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>

            <div class="original-gnavi"><?php dynamic_sidebar( 'SPグロナビ' ) ; ?></div>
        <?php endif; ?>

        <?php if (!is_mobile()): ?>
            <div class="main_visual">
                <a href="https://drawer.orilab.jp/index.html?url_site=https://nobori.orilab.jp">
                    <img src="http://stationary.orilab.jp/wp-content/themes/child0828/images/bungu_pc.jpg" alt="" />
                </a>
            </div>
        <?php else: ?>
            <div class="main_visual">
                <a href="https://drawer.orilab.jp/index.html?url_site=https://nobori.orilab.jp">
                    <img src="http://stationary.orilab.jp/wp-content/themes/child0828/images/bungu_sp.jpg" alt="" />
                </a>
            </div>
        <?php endif; ?>

        <!-- ヘッダー上部此処まで -->
        <!-- mobileのときは記事内1つめの画像、それ以外のときはアイキャッチ画像をヘッダー下に表示 -->
        <?php if (!is_mobile()): ?>
            <?php if (has_post_thumbnail()&&is_page()): ?>
                <div id="eye-catch">
                    <h1><?php the_post_thumbnail('full', array('alt' => "オリジナルスマホケースを1個から激安価格で作成-スマホケースラボ")); ?></h1>
                    <?php if (is_front_page()): ?>
                        <a href="#toiawase"><img class="order-button" src="/wp-content/uploads/2017/12/template_btn.png" alt="簡単！見積もり・相談はこちら！"></a>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        <?php else: ?>
            <?php if(catch_that_image() != null && has_post_thumbnail() &&is_page()): ?>
                <div id="eye-catch">
                    <h1><img src="<?php echo catch_that_image(); ?>" alt="オリジナルスマホケースを1個から激安価格で作成-スマホケースラボ"/></h1>
                    <?php if (is_front_page()): ?>
                        <a href="#toiawase"><img class="order-button" src="/wp-content/uploads/2017/12/template_btn.png" alt="簡単！見積もり・相談はこちら！"></a>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        <?php get_template_part('header-custom-field'); ?>
    </header><!-- #masthead -->

    <?php
    /**
     * Functions hooked in to storefront_before_content
     *
     * @hooked storefront_header_widget_region - 10
     */
    do_action( 'storefront_before_content' ); ?>

    <div id="content" class="site-content" tabindex="-1">


        <div class="col-full">

<?php
/**
 * Functions hooked in to storefront_content_top
 *
 * @hooked woocommerce_breadcrumb - 10
 */
do_action( 'storefront_content_top' );
