<?php

require_once("../../../../wp-load.php");

$base = substr($_GET['p'], 0, -10);
$path = base64_decode($base);

$upload_path = get_option('wpf_umf_upload_path');

$base_path = substr($path, 0, strlen($upload_path));

if (strtolower($base_path) != strtolower($upload_path)) {
  die('Permission denied');
}

$mime_type="application/octet-stream"; // modify accordingly to the file type of $_GET['path'], but in most cases no need to do so

if (file_exists($path)) {
    header('Content-Type: image/jpeg');
    readfile($path);
} else {
    die('File could not be found, is it deleted?');
}

exit();

?>
