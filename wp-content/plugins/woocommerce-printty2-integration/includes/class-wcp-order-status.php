<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WCP_OrderStatus{

	function __construct(){
		add_action( 'init', [$this, 'register_custom_order_status' ]);
		add_filter( 'wc_order_statuses', [ $this, 'add_custom_status_to_order_statuses' ]);
	}

	public function register_custom_order_status(){
		register_post_status( 'wc-awaiting-data', array(
			'label'                     => _x( 'Awaiting data', 'Order status', 'woocommerce-printty2-integration' ),
			'public'                    => true,
			'exclude_from_search'       => false,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop( 'awaiting data <span class="count">(%s)</span>', 'awaiting data<span class="count">(%s)</span>', 'woocommerce-printty2-integration')
		) );
		register_post_status( 'wc-processing-p2', array(
			'label'                     => _x('Processing(Printty2)', 'Order status', 'woocommerce-printty2-integration' ),
			'public'                    => true,
			'exclude_from_search'       => false,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop( 'Processing(Printty2)<span class="count">(%s)</span>', 'Processing(Printty2) <span class="count">(%s)</span>', 'woocommerce-printty2-integration' )
		) );
		register_post_status( 'wc-sended-p2', array(
			'label'                     => _x('Sended(Printty2)', 'Order status', 'woocommerce-printty2-integration' ),
			'public'                    => true,
			'exclude_from_search'       => false,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop( 'Sended(Printtry2)<span class="count">(%s)</span>', 'Sended(Printtry2)<span class="count">(%s)</span>', 'woocommerce-printty2-integration')
		) );
		register_post_status( 'wc-failed-p2', array(
			'label'                     => _x('Failed(Printty2)', 'Order status', 'woocommerce-printty2-integration' ),
			'public'                    => true,
			'exclude_from_search'       => false,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop( 'Failed(Printty2) <span class="count">(%s)</span>', 'Failed(Printty2)<span class="count">(%s)</span>', 'woocommerce-printty2-integration')
		) );
		register_post_status( 'wc-delivered', array(
			'label'                     => _x('Delivered(Printty2)', 'Order status', 'woocommerce-printty2-integration' ),
			'public'                    => true,
			'exclude_from_search'       => false,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop( 'Delivered<span class="count">(%s)</span>', 'Delivered<span class="count">(%s)</span>', 'woocommerce-printty2-integration')
		) );
	}

	public function add_custom_status_to_order_statuses( $order_statuses ) {

		$new_order_statuses = array();

		// add new order status after processing
		foreach ( $order_statuses as $key => $status ) {

			$new_order_statuses[ $key ] = $status;

			if ( 'wc-on-hold' === $key ) {
				$new_order_statuses['wc-awaiting-data'] = _x('Awaiting data', 'Order status', 'woocommerce-printty2-integration' );
			}
			if ( 'wc-completed' === $key ) {
				$new_order_statuses['wc-processing-p2'] = _x('Processing(Printty2)', 'Order status', 'woocommerce-printty2-integration' );
				$new_order_statuses['wc-sended-p2'] = _x('Sended(Printty2)', 'Order status', 'woocommerce-printty2-integration' );
				$new_order_statuses['wc-failed-p2'] = _x('Failed(Printty2)', 'Order status', 'woocommerce-printty2-integration' );
			}
			if ( 'wc-refunded' === $key ) {
				$new_order_statuses['wc-delivered'] = _x('Delivered', 'Order status', 'woocommerce-printty2-integration' );
			}
		}

		return $new_order_statuses;
	}
}

new WCP_OrderStatus();
