<?php

if ( ! defined( 'ABSPATH' ) ) {
	EXIT;
}

class WCP_OrderTask {

	function __construct() {
		#add_action('wcprintty2_update_order_status', [$this,  'update_ordered_orders_printty2_status']);
		#add_action('admin_menu', [$this,  'update_ordered_orders_printty2_status']);
	}

	// printtry2への注文のステータスを更新する。
	public function update_ordered_orders_printty2_status(){
		try {
			if ( ! $this->is_execute_time() ){
				wcp2i()->log("スケジュール実行をスキップします");
				return;
			}

			wcp2i()->log("スケジュール実行::WCP_OrderTask:update_ordered_orders_printty2_status");
			WCP_Order::update_orders_status_printty2();

			$file = WCP_ABSPATH . "/logs/task_last_run.txt";
			touch($file);
		}catch (Exception $e){
			wcp2i()->log($e->getMessage());
			wcp2i()->log($e->getTraceAsString());
		}
	}

	// 
	public function send_mail_request_customers_voice(){
		$process_hour = wcp2i()->get_date('H');
		if(strcmp($process_hour, '18') != 0){
			wcp2i()->log("アンケートメールは、18時以外スキップします。");
			return false;
		}

		try {
			$orders = $this->request_customers_voice_users();
			if ( count($orders) > 0 ){
				foreach ( $orders as $o ) {
					$order = new WCP_Order($o);
					if ($order->get_printty2_sended_request_customers_voice()){
						wcp2i()->log("アンケートメール配信済みのためスキップします。: ".$order->get_id());
					}else{
						wcp2i()->log("アンケートメールを配信します。: ".$order->get_id());
						WC()->mailer()->emails['WCP_Email_Customer_Request_Customers_Voice']->trigger( $order->get_id(), $order );

						$order->set_printty2_sended_request_customers_voice(True);
						$order->save();
					}
				}
			}else{
				wcp2i()->log("一件も注文がみつかりませんでした。");
			}
		}catch (Exception $e){
			wcp2i()->log($e->getMessage());
			wcp2i()->log($e->getTraceAsString());
		}
	}

	private function request_customers_voice_users(){
		$sended_date = wcp2i()->get_date('Y-m-d', strtotime("- 3 day"));

		wcp2i()->log($sended_date . "に配送された注文を検索。");
		$args = array(
			'printty2_sended_date' => $sended_date 
		);
		return wc_get_orders( $args );
	}

	private function is_execute_time(){
		$file = WCP_ABSPATH . "/logs/task_last_run.txt";
		if ( !file_exists($file) ) {
			touch($file);
		}

		$diff = (new DateTime())->diff( new DateTime(date('Y/m/d H:i:s', filemtime($file))) );
		return  $diff->h > 0;
	}
}
new WCP_OrderTask();
