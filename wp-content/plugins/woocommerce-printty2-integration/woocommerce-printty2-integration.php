<?php
/*
Plugin Name: Woocommerce Printty2 Integration
Plugin URI: http://spicelife.jp/
Description: wordpressとprintty2の連携機能を提供します。
Version: 0.8
Author: spicelife
Author URI: http://spicelife.jp/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Define WC_PLUGIN_FILE.
if ( ! defined( 'WCP_PLUGIN_FILE' ) ) {
	define( 'WCP_PLUGIN_FILE', __FILE__ );
}

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) && ! class_exists( 'WooCommercePrintty2Integration' )) :

	/**
	 * Main WooCommercePrintty2Integration Class.
	 *
	 * @class WooCommercePrintty2Integration
	 * @version	0.1
	 */
	final class WooCommercePrintty2Integration {

		/**
		 * The single instance of the class.
		 *
		 * @var WooCommercePrintty2Integration
		 * @since 2.1
		 */
		protected static $_instance = null;

		/**
		 * WooCommerce Constructor.
		 */
		private function __construct() {
			$this->define_constants();
			$this->includes();
			$this->init_hooks();
		}

		/**
		 * Define WC Constants.
		 */
		private function define_constants() {
			$this->define( 'WCP_ABSPATH', dirname( WCP_PLUGIN_FILE ) . '/' );
			$this->define( 'WCP_PLUGIN_BASENAME', plugin_basename( WCP_PLUGIN_FILE ) );
		}

		/**
		 * Define constant if not already set.
		 *
		 * @param  string $name
		 * @param  string|bool $value
		 */
		private function define( $name, $value ) {
			if ( ! defined( $name ) ) {
				define( $name, $value );
			}
		}

		/**
		 * Include required core files used in admin and on the frontend.
		 */
		public function includes() {
			include_once( WCP_ABSPATH . 'includes/class-wcp-install.php' );
			# Core
			include_once( WCP_ABSPATH . 'includes/class-wcp-order-status.php' );
			include_once( WCP_ABSPATH . 'includes/class-wcp-order.php' );
			include_once( WCP_ABSPATH . 'includes/class-wcp-order-item-product.php' );
			include_once( WCP_ABSPATH . 'includes/data-stores/class-wcp-data-store-ex.php' );

			# API
			include_once( WCP_ABSPATH . 'includes/class-wcp-ajax.php' );
			include_once( WCP_ABSPATH . 'includes/printty2/master/class-printty2-master-base.php' );
			include_once( WCP_ABSPATH . 'includes/printty2/master/class-printty2-item.php' );
			include_once( WCP_ABSPATH . 'includes/printty2/master/class-printty2-item-color.php' );
			include_once( WCP_ABSPATH . 'includes/printty2/master/class-printty2-item-size.php' );
			include_once( WCP_ABSPATH . 'includes/printty2/master/class-printty2-item-image-size.php' );
			include_once( WCP_ABSPATH . 'includes/printty2/master/class-printty2-item-side.php' );
			include_once( WCP_ABSPATH . 'includes/printty2/api/class-restclient.php' );
			include_once( WCP_ABSPATH . 'includes/printty2/api/class-orders.php' );


			# Admin
			if ( $this->is_request( 'admin' ) ) {
				# post list
				# include_once( WCP_ABSPATH . 'includes/admin/class-wcp-admin-post-types.php' );

				# custom post page
				include_once( WCP_ABSPATH . 'includes/admin/class-wcp-admin-notice.php' );
				include_once( WCP_ABSPATH . 'includes/admin/class-wcp-admin-meta-boxes.php' );
				include_once( WCP_ABSPATH . 'includes/admin/class-wcp-meta-box-order-data.php' );
				include_once( WCP_ABSPATH . 'includes/admin/class-wcp-meta-box-order-items.php' );
				include_once( WCP_ABSPATH . 'includes/admin/class-wcp-meta-box-printty2-status.php' );
				include_once( WCP_ABSPATH . 'includes/admin/class-wcp-meta-box-order-invoice.php' );
				include_once( WCP_ABSPATH . 'includes/admin/class-wcp-meta-box-delivery-source.php' );

				# Email
				include_once( WCP_ABSPATH . 'includes/class-wcp-emails.php' );

				# Task
				include_once( WCP_ABSPATH . 'includes/task/class-wcp-order-task.php' );
			}
		}

		/**
		 * Hook into actions and filters.
		 */
		private function init_hooks() {
			register_activation_hook( __FILE__, array( 'WCP_Install', 'install' ) );
			register_deactivation_hook( __FILE__, array( 'WCP_Install', 'uninstall' ) );

			add_action( 'init', array( $this, 'init' ), 0 );
		}
		/**
		 * Init WooCommerce when WordPress Initialises.
		 */
		public function init() {
			// Set up localisation.
			$this->load_plugin_textdomain();
		}

		/**
		 * Load Localisation files.
		 */
		public function load_plugin_textdomain() {
			$locale = is_admin() && function_exists( 'get_user_locale' ) ? get_user_locale() : get_locale();
			$locale = apply_filters( 'plugin_locale', $locale, 'woocommerce-printty2-integration' );
			load_plugin_textdomain( 'woocommerce-printty2-integration', false, plugin_basename( dirname( __FILE__ ) ) . '/i18n/languages/' );
		}

		/**
		 * Get the plugin path.
		 *
		 * @return string
		 */
		public function plugin_path() {
			return untrailingslashit( plugin_dir_path( WCP_PLUGIN_FILE ) );
		}

		/**
		 * What type of request is this?
		 *
		 * @param  string $type admin, ajax, cron or frontend.
		 * @return bool
		 */
		private function is_request( $type ) {
			switch ( $type ) {
			case 'admin' :
				return is_admin();
			case 'ajax' :
				return defined( 'DOING_AJAX' );
			case 'cron' :
				return defined( 'DOING_CRON' );
			case 'frontend' :
				return ( ! is_admin() || defined( 'DOING_AJAX' ) ) && ! defined( 'DOING_CRON' );
			}
		}

		public function get_date($format, int $timestamp = null ){
			if (null === $timestamp) {
				$timestamp = time();
			}

			# get jst. swap timezone
			$org_timezone = date_default_timezone_get();
			date_default_timezone_set('Asia/Tokyo');
			$date = date($format, $timestamp);
			date_default_timezone_set($org_timezone);

			return $date;
		}

		public function log( $msg ){
			$db = debug_backtrace()[1];
			$msg = $this->get_date("[Y/m/d H:i:s]") . $db['class'] . '#' . $db['function']. ' ' . $msg . "\n";
			$date = $this->get_date("Ymd");

			error_log($msg, 3, WCP_ABSPATH . "/logs/wpintegration_" . $date . ".log");
		}

		/**
		 * Main WooCommerce Printty2 Integrations Instance.
		 *
		 * Ensures only one instance of WooCommercePrintty2Integration is loaded or can be loaded.
		 *
		 * @since 2.1
		 * @static
		 * @see WCP2I()
		 * @return WooCommerce Printty2 Integrations- Main instance.
		 */
		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
			}
			return self::$_instance;
		}
	}

endif;

/**
 * Main WooCommerce Printty2 Integrations Instance.
 *
 * Returns the main instance of WC to prevent the need to use globals.
 *
 * @since  2.1
 * @return WooCommerce
 */
function wcp2i() {
	return WooCommercePrintty2Integration::instance();
}

wcp2i();
