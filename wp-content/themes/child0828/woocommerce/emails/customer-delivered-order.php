<?php
/**
 * Customer processing order printty2 email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-processing-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>
<p>
≪ご注文番号: <?= $order->get_id(); ?>≫<br/>
<br/>
<?= $order->get_formatted_billing_full_name() ?>様<br/>
<br/>
【オリジナルスマホケースラボ】のご利用ありがとうございます。<br>
ご注文の商品を出荷致しましたのでお知らせ致します。<br>
★商品が届きましたら、以下の内容確認をお願いします。<br>
・商品の内容に誤りはないか<br>
・色、枚数は希望どおりか<br>
・破損などしていないか<br>
・汚れや傷がないか<br>
<br>
※届いた商品に問題がありましたら、商品到着後1週間以内にサポートデスクへご連絡くださいませ。<br>

■お客様の声とお写真をサイトに掲載しませんか？
オリジナルスマホケースラボでは、お客様に作成頂いたスマホケースのお写真やお客様の声をサイトに掲載しております。
もしよろしければ、今回届いた商品の感想をぜひお聞かせください。
URL:https://form.jotform.me/80031950580450

スタッフ一同お待ちしております。

■ 発送情報<br>
…………………………………………………………………………………………<br>
お問い合わせNO. <?= (new WCP_Order($order))->get_printty2_tracking_number() ?>(<?= (new WCP_Order($order))->get_delivery_service_name()?>)<br>

<?php
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );
?>

■ご注文内容<br>
<?php

/**
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );
?>

<br>
ご注文に関する不明点、疑問点などお問い合わせに関しましては<br>
以下のお問い合わせ窓口よりご連絡ください。<br>
<br>
★お問い合わせの前にこちらもご確認ください。<br>
FAQ（よくある質問）　<a href="https://original-smaphocase.com/info/faq">https://original-smaphocase.com/info/faq</a><br>
<br>
このEメールアドレスは、配信専用です。このメッセージに返信しないようお願いいたします。<br>
<br>
ご利用いただきありがとうございました。<br>
今後ともオリラボをよろしくお願いいたします。<br>
―スマホラボ―――――――――――――――――――――<br>
URL： <a href="https://original-smaphocase.com/">https://original-smaphocase.com/</a><br>
お問い合わせ： <a href="https://original-smaphocase.com/contact">https://original-smaphocase.com/contact</a><br>
電話（月〜金11時から17時まで）： 0120-570-150 （姉妹サイトTMIXサポートデスクで対応致します。）<br>11
運営会社 ： 株式会社spice life<br>
―――――――――――――――――――――――――<br>
</p>

<?php
/**
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );

