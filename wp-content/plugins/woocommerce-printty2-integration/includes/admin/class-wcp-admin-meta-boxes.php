<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * WCP_Admin_Meta_Boxes.
 */
class WCP_Admin_Meta_Boxes {

	public function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 40 );

		# Save Order Metabox
		add_action( 'woocommerce_process_shop_order_meta', 'WCP_Meta_Box_Order_Data::save', 50, 2 );
		add_action( 'woocommerce_process_shop_order_meta', 'WCP_Meta_Box_Order_Delivery_Source::save', 50, 2 );
	}

	/**
	 * Add WCP Meta boxes.
	 */
	public function add_meta_boxes() {
		foreach ( wc_get_order_types( 'order-meta-boxes' ) as $type ) {
			$order_type_object = get_post_type_object( $type );

			add_meta_box( 'printty2_invoice',
				__( 'Register Invoice', 'woocommerce-printty2-integration' ),
				'WCP_Meta_Box_Order_Invoice::output', $type, 'side', 'high' );

			add_meta_box( 'printty2_situation',
				__( 'Printty2 Situation', 'woocommerce-printty2-integration' ),
				'WCP_Meta_Box_Printty2_Status::output', $type, 'side', 'high' );

			add_meta_box( 'printty2_delivery_source',
				__( 'Delivery Source', 'woocommerce-printty2-integration' ),
				'WCP_Meta_Box_Order_Delivery_Source::output', $type, 'side', 'high' );
		}
	}
}
new WCP_Admin_Meta_Boxes();
