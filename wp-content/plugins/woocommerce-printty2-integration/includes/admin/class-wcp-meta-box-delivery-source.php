<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class WCP_Meta_Box_Order_Delivery_Source{
	protected static $printty2_status_fields = array();

	public static function save( $order_id ) {
		$order = new WCP_Order(wc_get_order( $order_id )); 

		$order->set_delivery_source_company_for_printty2($_POST['_delivery_source_company_for_printty2']);
		$order->set_delivery_source_postcode_for_printty2($_POST['_delivery_source_postcode_for_printty2']);
		$order->set_delivery_source_state_for_printty2($_POST['_delivery_source_state_for_printty2']);
		$order->set_delivery_source_city_for_printty2($_POST['_delivery_source_city_for_printty2']);
		$order->set_delivery_source_address1_for_printty2($_POST['_delivery_source_address1_for_printty2']);
		$order->set_delivery_source_address2_for_printty2($_POST['_delivery_source_address2_for_printty2']);
		$order->set_delivery_source_phonenumber_for_printty2($_POST['_delivery_source_phonenumber_for_printty2']);

		$order->save();
	}

	public static function output( $post ) {
		global $theorder;

		if ( ! is_object( $theorder ) ) {
			$theorder = wc_get_order( $post->ID );
		}
		$order = new WCP_Order($theorder);


?>
<div class="panel-wrap woocommerce">
	<div id="delivery_source_data" class="panel">
		<div class="order_data_column_container">
			<div class="order_data_column">
				<p class="form-field">
※「株式会社spice life」とした場合はspicelifeとして配送元を設定します。
					<label>企業名</label>
					<input id="_delivery_source_company_for_printty2" name="_delivery_source_company_for_printty2" type="text" class="short" value="<?= esc_attr($order->get_delivery_source_company_for_printty2()) ?>"></input>
				</p>


				<p class="form-field">
					<label>郵便番号</label>
					<input id="_delivery_source_postcode_for_printty2" name="_delivery_source_postcode_for_printty2" type="text" class="short" value="<?= esc_attr($order->get_delivery_source_postcode_for_printty2()) ?>"></input>
				</p>

				<p class="form-field form-field-wide">
					<label>都道府県</label>
					<select id="_delivery_source_state_for_printty2" name="_delivery_source_state_for_printty2" class="wc-enhanced-select">
				<?php
						//TODO: JP決め打ち
						foreach ( WC()->countries->get_states('JP') as $status => $status_name ) {
							echo '<option value="' . esc_attr($status) . '" ' . selected( $status, $order->get_delivery_source_state_for_printty2(), false ) . '>' . esc_html( $status_name ) . '</option>';

						}
				?>
					</select>
				</p>

				<p class="form-field">
					<label>市区町村</label>
					<input id="_delivery_source_city_for_printty2" name="_delivery_source_city_for_printty2" type="text" class="short" value="<?= esc_attr($order->get_delivery_source_city_for_printty2()) ?>"></input>
				</p>

				<p class="form-field">
					<label>住所詳細１</label>
					<input id="_delivery_source_address1_for_printty2" name="_delivery_source_address1_for_printty2" type="text" class="short" value="<?= esc_attr($order->get_delivery_source_address1_for_printty2()) ?>"></input>
				</p>

				<p class="form-field">
					<label>住所詳細２</label>
					<input id="_delivery_source_address2_for_printty2" name="_delivery_source_address2_for_printty2" type="text" class="short" value="<?= esc_attr($order->get_delivery_source_address2_for_printty2()) ?>"></input>
				</p>

				<p class="form-field">
					<label>電話番号</label>
					<input id="_delivery_source_phonenumber_for_printty2" name="_delivery_source_phonenumber_for_printty2" type="text" class="short" value="<?= esc_attr($order->get_delivery_source_phonenumber_for_printty2()) ?>"></input>
				</p>

			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<?php
	}
}

