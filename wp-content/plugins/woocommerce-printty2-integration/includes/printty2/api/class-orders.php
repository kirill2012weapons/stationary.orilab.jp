<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @class   Orders
 */
class WCP_Printty2API_Orders {
  const API_KEY = "59a39a72-efec-40d2-9d17-1646ac1f104c";

	private static $instance;
	private static $client;

	public static function getInstance() {
		if (!isset(self::$instance)) {
			self::$instance = new WCP_Printty2API_Orders();
		}

		return self::$instance;
	}

	public final function __clone() {
		throw new RuntimeException ('Clone is not allowed against ' . get_class($this));
	}

	function __construct() {
		self::$client = new WCP_Printty2API_RestClient([
			'base_url' => "http://api.beta.printty.maruig.com/"
		]);
	}

	public function orders($page = 1, $size = 1000){
		#$result = self::$client->get("external/orders", []);
		#$json = '{"orders":[{"Order":{"id":200,"number":"P123456789","date":"2016-12-31","production_date_preferred":"2017-01-01","status_code":"InProgress","amount":1234}}],"total_count":4}';

		$page = $page - 1;
		if ( $page < 0 ) $page = 0;

		$params = array(
			'api_key' => self::API_KEY,
			'paging_size' => $size,
			'paging_offset' => $size * $page,
			'status_id' => 1
		);

		$result = self::$client->get("external/orders", $params);
		return json_decode($result->response);
	}

	public function create(WCP_Printty2API_Order_Parameter $param){
		#TODO: this is test
		#return json_decode('{"Order":{"id":42755, "number": "20171121SL0003"}}');

		$result = self::$client->put("external/orders/order", $param->toJson());
		return json_decode($result->response);
	}

	public function fetch($order_id){
		$params = array(
			'api_key' => self::API_KEY,
			"order_id" => $order_id
		);
		$result = self::$client->get("external/orders/order", $params);
		return json_decode($result->response);
	}

	public function delete($order_id){
		#return true;
		$params = array(
			'APIKey' => self::API_KEY,
			'Order' => array( 'id' => $order_id )
		);

		$result = self::$client->delete("external/orders/order",  json_encode($params));
		return json_decode($result->response);
	}
}

class WCP_Printty2API_Order_Parameter implements ArrayAccess {
	private $values;

	function __construct(array $value = array()){
		$this->values = $value;
	}

	// パラメータを生成
	static public function build($order){
		# order
		# billing
		$p_order = array (
			'billing' => array (
				'company' => $order->get_delivery_source_company_for_printty2(),
				'address' => array (
					'postcode' => $order->get_delivery_source_postcode_for_printty2(),
					'state' => $order->get_delivery_source_state_name_for_printty2(),
					'city' => $order->get_delivery_source_city_for_printty2(),
					'address1' => $order->get_delivery_source_address1_for_printty2(),
					'address2' => $order->get_delivery_source_address2_for_printty2(),
				),
				'phonenumber' => $order->get_delivery_source_phonenumber_for_printty2(),
				'delivery_service_account_code' => $order->get_delivery_service_account_code(),
			),
			'shipping' => array (
				'company' => $order->get_shipping_company_for_printty2(),
				'address' => array (
					'postcode' => $order->get_shipping_postcode(),
					'state' => $order->get_shipping_state_name(),
					'city' => $order->get_shipping_city(),
					'address1' => $order->get_shipping_address_1(),
					'address2' => $order->get_shipping_address_2(),
				),
				'person' => $order->get_shipping_full_name(),
				'phonenumber' => $order->get_shipping_phone(),
				'email' => $order->get_billing_email(),
				'delivery_period_id' => $order->get_shipping_delivery_period_id(),
			),
			'use_package' => $order->get_use_package(),
			'use_original_tag' => 0,
		);

		$p_items = array();
		# wc_order
		foreach ( $order->get_product_items() as $i) {
			$p = new WCP_Order_Item_Product($i);

			$p_items[] = array(
				'OrderItem' => array('quantity' => $p->get_quantity()),
				'Product' => array( 'id' => $p->get_printty2_product_id()),
				'ProductSize' =>  array( 'id' => $p->get_product_size_id()),
				'ProductColor' => array( 'id' => $p->get_product_color_id()),
				'tasks' => array(
					array(
						'OrderItemTask' => array(
							'image_url' => $p->get_image_url(),
							'preview_image_url' => $p->get_preview_image_url(),
						),
						'ProductColorSide' => array( 'id' => $p->get_product_color_side_id() ),
					),
				),
				'optional_keys' => array(),
			);
		}

		$param = array (
			'APIKey' => WCP_Printty2API_Orders::API_KEY,
			'Order'=> $p_order,
			'PriorityType' => array( 'code' => $order->get_priority_type_code() ),
			'DeliveryType' => array( 'code' => $order->get_delivery_type_code() ),
			'DeliveryService' => array( 'code' => $order->get_delivery_service_code() ),
			'optional_keys' => array (
				array(
					'OrderOptionalKey' => array( 'key' => 'customer_order_number', 'value' => $order->get_id() ),
				),
				array(
					'OrderOptionalKey' => array( 'key' => 'delivery_note_url', 'value' => $order->get_invoice_url() ),
				)
			),
			'items' => $p_items,
		);
		return new WCP_Printty2API_Order_Parameter($param);
	}

	public function offsetExists($offset) {
		return isset($this->values[$offset]);
	}

	public function offsetGet($offset) {
		if (isset($this->values[$offset])) {
			return $this->values[$offset];
		}
		return null;
	}

	public function offsetSet($offset, $value) {
		$this->values[$offset] = $value;
	}

	public function offsetUnset($offset) {
		unset($this->values[$offset]);
	}

	public function toJson(){
		return json_encode($this->values);
	}

	public function __toString(){
		return json_encode($this->values, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
	}
}
