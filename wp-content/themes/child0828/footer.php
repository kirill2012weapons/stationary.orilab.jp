<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'storefront_before_footer' ); ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="col-full">

			<?php
			/**
			 * Functions hooked in to storefront_footer action
			 *
			 * @hooked storefront_footer_widgets - 10
			 * @hooked storefront_credit         - 20
			 */
			do_action( 'storefront_footer' ); ?>

		</div><!-- .col-full -->
	</footer><!-- #colophon -->


<!--div id="sidemenu"><a href="https://orilab.jp/designenq/" target="_blank"><img src="/wp-content/uploads/2017/12/bnr_maru.png" style="width: 60%;"></a></div-->
<!--div id="sidemenu"><a href="https://orilab.jp/designenq/" target="_blank"><img src="/wp-content/uploads/2018/02/head-btn-soudan2.png"></a></div-->
<div class="pc">
<div id="sidemenu"><a href="https://drawer.orilab.jp/index.html?url_site=https://original-smaphocase.com" target="_blank"><img src="/wp-content/uploads/2018/06/dezasapo.png"></a></div>
</div>
<div class="sp">
<div id="sidemenu"><a href="https://drawer.orilab.jp/index.html?url_site=https://original-smaphocase.com" target="_blank"><img src="/wp-content/uploads/2018/06/dezasapo_sp.png"></a></div>
</div>
	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
