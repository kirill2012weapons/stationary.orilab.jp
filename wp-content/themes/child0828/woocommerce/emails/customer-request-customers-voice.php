<?php
/**
 * Request Customer's Voice
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-request-customers-voice.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>
<p>
≪ご注文番号: <?= $order->get_id(); ?>≫<br/>
<br/>
<?= $order->get_formatted_billing_full_name() ?>様<br/>
<br/>
【オリジナルスマホケースラボ】のご利用ありがとうございます。<br>

先日お求めいただいた商品にご満足いただけたでしょうか。お買い上げいただいた商品に関するお客様のご感想をお聞かせください。
今後の商品開発、サービス運営の参考とさせていただきます。

※ アンケートは満足度を簡単な内容となっており、2~3分程度です。

<a href="https://form.jotform.me/80031950580450">https://form.jotform.me/80031950580450</a>

万一、「商品が届かない」「配送中に袋がやぶれていた」などの配送上のトラブルがありましたら、
上記アンケートに記載いただけますと幸いです。

<br>
ご注文に関する不明点、疑問点などお問い合わせに関しましては<br>
以下のお問い合わせ窓口よりご連絡ください。<br>
<br>
★お問い合わせの前にこちらもご確認ください。<br>
FAQ（よくある質問）　<a href="https://original-smaphocase.com/info/faq">https://original-smaphocase.com/info/faq</a><br>
<br>
このEメールアドレスは、配信専用です。このメッセージに返信しないようお願いいたします。<br>
<br>
ご利用いただきありがとうございました。<br>
今後ともオリラボをよろしくお願いいたします。<br>
―スマホラボ―――――――――――――――――――――<br>
URL： <a href="https://original-smaphocase.com/">https://original-smaphocase.com/</a><br>
お問い合わせ： <a href="https://original-smaphocase.com/contact">https://original-smaphocase.com/contact</a><br>
電話（月〜金9時から18時まで）： 0120-570-150 （姉妹サイトTMIXサポートデスクで対応致します。）<br>
運営会社 ： 株式会社spice life<br>
―――――――――――――――――――――――――<br>
</p>

<?php
/**
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );

