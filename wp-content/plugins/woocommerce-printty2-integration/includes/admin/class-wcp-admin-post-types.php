<?php

/**
 * WC_Admin_Post_Ex_Types Class.
 *
 * @class WCP_Admin_Post_Ex_Types
 */
final class WCP_Admin_Post_Types {
	public function __construct() {
		# カラム追加する場合は以下を有効かする
		#add_filter( 'manage_shop_order_posts_columns', array( $this, 'shop_order_columns' ), 20, 2 );
		#add_action( 'manage_shop_order_posts_custom_column', array( $this, 'render_shop_order_columns' ));

		add_filter( 'woocommerce_admin_order_actions', array( $this, 'filter_woocommerce_admin_order_actions' ));
	}

	/**
	 * Define custom columns for orders.
	 * @param  array $existing_columns
	 * @return array
	 */
	public function shop_order_columns( $columns ) {
		$columns['post_modified'] = "修正日";

		return $columns;
	}

	/**
	 * Add custom action for shop order.
	 * @param string $column
	 */
	public function filter_woocommerce_admin_order_actions( $actions, $the_order = null){
		global $post;

		$actions['printty2'] = array(
			'url'       => wp_nonce_url( admin_url( 'admin-ajax.php?action=woocommerce_printty2_order_create&order_id=' . $post->ID ), 'woocommerce_printty2_order_create' ),
			'name'      => 'Printty2へ発注',
			'action'    => 'Printty2',
		);

		return $actions;
	}

	/**
	 * Output custom columns for shop order.
	 * @param string $column
	 */
	public function render_shop_order_columns( $column ) {
		global $post, $the_order;

		if ( empty( $the_order ) || $the_order->get_id() !== $post->ID ) {
			$the_order = wc_get_order( $post->ID );
		}

		switch ( $column ) {
		case 'post_modified' :

?>hoge
<?php
			break;
		}
	}
}
new WCP_Admin_Post_Types();
