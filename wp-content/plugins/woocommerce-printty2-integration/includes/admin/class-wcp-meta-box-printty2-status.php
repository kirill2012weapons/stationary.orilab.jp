<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class WCP_Meta_Box_Printty2_Status{
	protected static $printty2_status_fields = array();
	public static function init_printty2_fields() {
		self::$printty2_status_fields = array(
			'printty2_id' => array(
				'label' => __( 'Printty2 ID', 'woocommerce-printty2-integration' ),
				'show'  => true,
			),
			'printty2_number' => array(
				'label' => __( 'Printty2 Number', 'woocommerce-printty2-integration' ),
				'show'  => true,
			),
			'printty2_date' => array(
				'label' => __( 'Printty2 Date', 'woocommerce-printty2-integration' ),
				'show'  => true,
			),
			'printty2_production_date_preferred' => array(
				'label' => __( 'Production Date Preferred', 'woocommerce-printty2-integration' ),
				'show'  => true,
			),
			'printty2_status_name' => array(
				'label' => __( 'Printty2 Status', 'woocommerce-printty2-integration' ),
				'show'  => true,
			),
			'printty2_amount' => array(
				'label' => __( 'Printty2 Amount', 'woocommerce-printty2-integration' ),
				'show'  => true,
			),
			'delivery_service_name' => array(
				'label' => __( 'Delivery Service', 'woocommerce-printty2-integration' ),
				'show'  => true,
			),
			'printty2_tracking_number' => array(
				'label' => __( 'Tracking Number', 'woocommerce-printty2-integration' ),
				'show'  => true,
			),
			'printty2_image_status' => array(
				'label' => __( 'Image Status', 'woocommerce-printty2-integration' ),
				'show'  => true,
			),
		);
	}


	public static function admin_printty2_order_actions($order){
		$actions = array();
		if (!$order->is_ordered_printty2()){
			$actions['create_printty2'] = array(
				'url'       => wp_nonce_url( admin_url( 'admin-ajax.php?action=woocommerce_printty2_order_create&order_id=' . $order->get_id() ), 'woocommerce_printty2_order_create' ),
				'name'      => __( 'Order to Printty2', 'woocommerce-printty2-integration' ),
			);
		}else{
			$actions['update_order_status_printty2'] = array(
				'url'       => wp_nonce_url( admin_url( 'admin-ajax.php?action=woocommerce_printty2_update_order_status&order_id=' . $order->get_id() ), 'woocommerce_printty2_update_order_status' ),
				'name'      => __( 'Update Order Status', 'woocommerce-printty2-integration' ),
			);

			#$actions['fetch_order_printty2'] = array(
			#	'url'       => wp_nonce_url( admin_url( 'admin-ajax.php?action=woocommerce_printty2_fetch_order&order_id=' . $order->get_id() ), 'woocommerce_printty2_fetch_order' ),
			#	'name'      => __( 'Fetch Order', 'woocommerce-printty2-integration' ),
			#);

			# 削除APIが動作しないので、一旦無効化
			#$actions['delete_order_printty2'] = array(
			#	'url'       => wp_nonce_url( admin_url( 'admin-ajax.php?action=woocommerce_printty2_delete_order&order_id=' . $order->get_id() ), 'woocommerce_printty2_delete_order' ),
			#	'name'      => __( 'Delete Order', 'woocommerce-printty2-integration' ),
			#);
		}
		if(array_key_exists("debug", $_GET) && strcmp($_GET["debug"], 'true') == 0){
			$actions['mail_test'] = array(
				'url'       => wp_nonce_url( admin_url( 'admin-ajax.php?action=woocommerce_printty2_mail_test&order_id=' . $order->get_id() ), 'woocommerce_printty2_mail_test' ),
				'name'      => __( 'MailTest', 'woocommerce-printty2-integration' ),
			);
			$actions['fetch'] = array(
				'url'       => wp_nonce_url( admin_url( 'admin-ajax.php?action=woocommerce_printty2_fetch_order&order_id=' . $order->get_id() ), 'woocommerce_printty2_fetch_order' ),
				'name'      => __( 'FetchOrder', 'woocommerce-printty2-integration' ),
			);
		}

		return $actions;
	}

	public static function output( $post ) {
		global $theorder;

		if ( ! is_object( $theorder ) ) {
			$theorder = wc_get_order( $post->ID );
		}
		$order = new WCP_Order($theorder);

		self::init_printty2_fields();

?>
<div class="panel-wrap woocommerce">
	<div id="printty2_status_data" class="panel">
		<div class="order_data_column_container">
			<div class="order_data_column">
				<table cellpadding="0" cellpadding="0" style="width:100%">
<?php
		foreach ( self::$printty2_status_fields as $key => $field ) {
			echo '<tr>';
			if ( isset( $field['show'] ) && false === $field['show'] ) {
				continue;
			}
			echo '<td><strong>' . esc_html( $field['label'] ). '</strong></td>';

			$method = 'get_'.$key;
			$field_value = $order->$method();
			if ( empty($field_value) ){
				echo '<td style="text-align:center;">----</td>';
			}else{
				echo '<td style="text-align:center;">' . wp_kses_post( $field_value ) . '</td>';
			}
			echo '</tr>';
		}
?>
				</table>
			</div>
		</div>
		<hr>
		<div>
<?php
		foreach ( self::admin_printty2_order_actions($order) as $key => $value ) {
			echo '<a style="margin:5px" class="button Printty2" href="' . $value['url'] . '">' . $value['name'] . '</a>';
		}
?>
		</div>
<?php if( !empty($order->get_printty2_message())){ ?>
		<div style="color:red;padding-top:5px;">最後のエラー</div>
		<div style="color:red;background-color: #ffc3c3; margin: 5px;padding: 5px;">
<?php
			echo str_replace("\n", '<br/>', $order->get_printty2_message());
?>
		</div>
<?php } ?>

		<div class="clear"></div>
	</div>
</div>
<?php
	}
}

