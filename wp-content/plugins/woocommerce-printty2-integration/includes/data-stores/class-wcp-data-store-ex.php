<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WCP_Data_Store_Ex {

	protected $internal_meta_keys = array(
		'_printty2_id',
		'_printty2_sended_date',
	);

	public function __construct(){
		add_filter( 'woocommerce_get_wp_query_args', [ $this, 'add_filter_woocommerce_get_wp_query_args' ], 10, 2);
	}

	public function add_filter_woocommerce_get_wp_query_args($wp_query_args, $query_vars){
		foreach ( $query_vars as $key => $value ) {
			if ( in_array( '_' . $key, $this->internal_meta_keys ) ) {
				unset($wp_query_args[$key]);
				$wp_query_args['meta_query'][] = array(
					'key'     => '_' . $key,
					'value'   => $value,
					'compare' => '=',
				);
			}

		}
		return $wp_query_args;
	}
}
new WCP_Data_Store_Ex();

