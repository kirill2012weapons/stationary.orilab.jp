<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WCP_Order_Item_Product {
	public function __construct(WC_Order_Item_Product $product){
		$this->product = $product;
	}

	// image
	public function get_image_url($with_timestamp = false){
		$url =  $this->get_upload_url() . '/_printty2_image';
		if ($with_timestamp == true ) $url .= '?' . time();
		return $url;
	}

	public function get_image_path(){
		return $this->get_upload_dir() . '/_printty2_image';
	}

	public function exists_image(){
		return is_readable($this->get_image_path());
	}

	public function set_image($file){
		$this->save_item_image('_printty2_image', $file);
	}

	// preview image
	public function get_preview_image_url($with_timestamp = false){
		$url = $this->get_upload_url() . '/_printty2_preview_image';
		if ($with_timestamp == true ) $url .= '?' . time();
		return $url;
	}

	public function get_preview_image_path(){
		return $this->get_upload_dir() . '/_printty2_preview_image';
	}

	public function set_preview_image($file){
		$this->save_item_image('_printty2_preview_image', $file);
	}

	public function exists_preview_image(){
		return is_readable($this->get_preview_image_path());
	}

	// upload dir
	public function get_upload_dir(){
		return WP_CONTENT_DIR . '/uploads/printty2_upload/item_' . $this->get_id();
	}

	public function get_upload_url(){
		return content_url() . '/uploads/printty2_upload/item_' . $this->get_id();
	}

	public function get_printty2_product_id(){
		$product = $this->get_printty2_product();
		return is_null($product) ? null : $product->get_id();
	}

	public function get_printty2_product(){
		return WCP_Printty2_Item::find_by_id(intval($this->get_meta_or_product_attribute('商品ID')));
	}

	public function get_product_color_id(){
		$color = $this->get_product_color();
		return is_null($color) ? null : $color->get_id();
	}

	public function get_product_color(){
		return WCP_Printty2_Item_Color::find_by_product_id_and_title(
			$this->get_printty2_product_id(),
			$this->get_meta_or_product_attribute('カラー')
		);
	}

	public function get_product_size_id(){
		$size = $this->get_product_size();
		return is_null($size) ? null : $size->get_id();
	}

	public function get_product_size(){
		return WCP_Printty2_Item_Size::find_by_product_id_and_title(
			$this->get_printty2_product_id(),
			$this->get_meta_or_product_attribute('サイズ')
		);
	}

	public function get_product_color_side_id(){
		$side = $this->get_product_color_side();
		return is_null($side) ? null : $side->get_id();
	}

	public function get_product_color_side(){
		return  WCP_Printty2_Item_Side::find_by_color_id_and_title(
			$this->get_product_color_id(),
			$this->get_meta_or_product_attribute('プリント面')
		);
	}

	static public function get_data(){
		return array(
			'_printty2_image_url',
			'_printty2_preview_image_url'
		);
	}

	private function save_item_image($name, $image){
		$dir = $this->get_upload_dir();
		$file = $dir . '/' . $name;

		if($image["tmp_name"]){
			list($file_name,$file_type) = explode(".", $image['name']);

			if(!file_exists($dir)){
				if(! mkdir($dir,0755, true)){
					throw new RuntimeException("failed make dir : " . $dir);
				}
			}
			if (move_uploaded_file($image['tmp_name'], $file)) {
				chmod($file, 0644);
			}else{
				throw new RuntimeException("failed move file : " . $file);
			}
		}else{
			throw new RuntimeException("tmp_name not found!!!");
		}
		return true;
	}

	public function valid_for_order_to_printty2(){
		$errors = [];

		if ($this->get_quantity() <= 0){
			$errors[] = '個数は1以上の必要があります。';
		}

		// 各マスタの存在チェック
		$require_fields = array(
			'quantity' => '個数',
			'printty2_product_id' => '商品ID',
			'product_color_id' => 'カラー',
			'product_color_side_id' => 'プリント面',
			'product_size_id' => 'サイズ',
		);

		foreach( $require_fields as $field => $field_name ) {
			$method_name = 'get_' . $field;
			$value = strval($this->$method_name());
			if ($value == ''){
				$errors[$field] = '[' . $this->get_name() . ']' .  $field_name . 'は必須です。';
			}
		}

		$product = $this->get_printty2_product();
		// カラーマスタ値チェック
		// エラーメッセージ補足
		if(isset($errors['product_color_id'])){
			if(isset($product)){
				$names = array();
				foreach ( $product->get_item_colors() as $color ) {
					$names[] =  $color->get_title();
				}
				if (!empty($names)){
					$errors['product_color_id'] .= '「' . join(',', $names) . '」である必要があります。';
				}
			}
		}

		// サイズマスタ値チェック
		// エラーメッセージ補足
		if(isset($errors['product_size_id'])){
			if(isset($product)){
				$names = array();
				foreach ( $product->get_item_sizes() as $size ) {
					$names[] =  $size->get_title();
				}
				if (!empty($names)){
					$errors['product_size_id'] .= '「' . join(',', $names) . '」である必要があります。';
				}
			}
		}

		// プリント面マスタ値チェック
		// エラーメッセージ補足
		if(isset($errors["product_color_side_id"])){
			$color = $this->get_product_color();
			if(isset($color)){
				$names = array();
				foreach ( $color->get_item_sides() as $side ) {
					$names[] =  $side->get_title();
				}
				if (!empty($names)){
					$errors['product_color_side_id'] .= '「' . join(',', $names) . '」である必要があります。';
				}
			}
		}

		$require_fields = array(
			'image' => '発注用画像',
			'preview_image' => '発注用画像(プレビュー)',
		);
		foreach( $require_fields as $field => $field_name ) {
			$method_name = 'exists_' . $field;
			if (!$this->$method_name()){
				$errors[$field] = '[' . $this->get_name() . ']' .  $field_name . 'は必須です。';
			}
		}

		// 画像サイズチェック
		if(!isset($errors['image'])){
			if(isset($product)){
				$image_size = $product->get_item_image_size();
				if ( is_null($image_size) ) {
					// マスタに存在しない場合はひとまずチェックしない
					// $errors[] = '[' . $this->get_name() . ']' . "発注用画像はサイズマスタに登録されていない商品です。";
				}else{
					list($width, $height, $type, $attr ) = getimagesize($this->get_image_path());
					if ( $image_size->get_width() != $width || $image_size->get_height() != $height ){
						$errors[] = '[' . $this->get_name() . '] の発注用画像はサイズがマスタと異なります。' . $image_size->get_width() . 'x' . $image_size->get_height() . 'である必要があります。設定された画像は、' . $width . 'x' . $height . 'です。';
					}
				}
			}
		}

		return array_values($errors);
	}


	private function get_meta_or_product_attribute( $name ){
		$value = strval( $this->get_meta($name));
		if ( $value == '' ){
			$ret = $this->get_product_attribute($name);
		} else {
			$ret = $this->get_meta($name);
		}

		return  $ret;
	}

	/**
		* 商品側の値を取得を取得
		* @return [integer] 商品ID
	 */
	private function get_product_attribute( $name ){
		foreach ( $this->get_product()->get_attributes() as $attr ) {
			if (strcmp( $attr->get_name(), $name ) == 0 ){
				if (count($attr->get_options() >= 1)){
					return $attr->get_options()[0];
				}
			}
		}
		return null;
	}

	public function __call( $method, $parameters ) {
		if ( is_callable( array( $this->product, $method ) ) ) {
			$object = array_shift( $parameters );
			return call_user_func_array( array( $this->product, $method ), array_merge( array( &$object ), $parameters ) );
		}
	}
}
