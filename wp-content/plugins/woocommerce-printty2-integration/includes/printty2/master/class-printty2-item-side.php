<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @class  WCP_Printty2_Item_Side
 */
class WCP_Printty2_Item_Side extends WCP_Printty2_Master_Base {
	public function __construct($id, $color_id, $title, $image_url ){
		$this->id = $id;
		$this->color_id = $color_id;
		$this->title = $title;
		$this->image_url = $image_url;
	}

	static public function get_master_file_path(){
		return '/data/printty_item_sides.csv';
	}

	static public function find_by_color_id ($color_id) {
		$file = static::get_master_file();
		$records = array();
		foreach ($file as $line) {
			if(!is_null($line[0]) && strcmp($line[1], $color_id) == 0 ){
				$records[] =  new WCP_Printty2_Item_Side($line[0], $line[1], $line[2], $line[3]);
			}
		}
		return $records;
	}

	static public function find_by_color_id_and_title ($color_id, $title) {
		$file = static::get_master_file();
		foreach ($file as $line) {
			if(!is_null($line[0]) && strcmp($line[1], $color_id) == 0 && strcmp($line[2], $title) == 0 ){
				return new WCP_Printty2_Item_Side($line[0], $line[1], $line[2], $line[3]);
			}
		}
		return null;
	}
}
