<?php
/*
* Plugin Name: WooCommerce Uploads
* Plugin URI: http://www.wpfortune.com
* Description: Upload files in WooCommerce
* Version: 1.4.1
* Author URI: http://www.wpfortune.com/
* Author: WP Fortune
* Text Domain: woocommerce-uploads
* License: GPL version 2 or later - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
* WC requires at least: 2.6.0
* WC tested up to: 3.2.2
*/

/*  Copyright 2014  WP Fortune  (email : info@wpfortune.com)


    This program is free software; you can redistribute it and/or modify

    it under the terms of the GNU General Public License, version 2, as

    published by the Free Software Foundation.


    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program; if not, write to the Free Software

    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/



if (! defined('ABSPATH')) {
    exit;
}



require_once(plugin_dir_path(__FILE__) . 'classes/class-wpf-uploads.php');

require_once(plugin_dir_path(__FILE__) . 'includes/helper.php');



/*

 * Settings needed for correct use with the WPFortune API

 */



$settings_plugin_name      =    'WooCommerce Uploads';

$settings_plugin_version   =    '1.4.1';

$settings_plugin_id        =    'woocommerce-uploads'; // Needed to work with the WPFortune updater

$settings_plugin_slug      =    'wpf_umf';

$settings_plugin_file      =    plugin_basename(__FILE__);

$settings_plugin_dir       =    plugin_dir_path(__FILE__);

$settings_upgrade_url      =    'https://www.wpfortune.com';

$settings_renew_url        =    'https://www.wpfortune.com/my-account/';

$settings_docs_url         =    'https://www.wpfortune.com/documentation/plugins/woocommerce-uploads/';

$settings_support_url      =    'https://wpfortune.com/contact/';


$wpf_uploads_instance = new WPF_Uploads($settings_plugin_name, $settings_plugin_version, $settings_plugin_id, $settings_plugin_slug, $settings_plugin_dir, $settings_plugin_file, $settings_upgrade_url, $settings_renew_url, $settings_docs_url, $settings_support_url);
