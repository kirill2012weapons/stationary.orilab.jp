<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WooCommerce WCP_AJAX.
 *
 * AJAX Event Handler.
 *
 * @class    WCP_AJAX
 */
class WCP_AJAX {
	/**
	 * Hook in ajax handlers.
	 */
	public static function init() {
		add_action( 'init', array( __CLASS__, 'define_ajax' ), 0 );
		#add_action( 'template_redirect', array( __CLASS__, 'do_wcp_ajax' ), 0 );
		self::add_ajax_events();
	}

	/**
	 * Set WCP AJAX constant and headers.
	 */
	public static function define_ajax() {
		if ( ! empty( $_GET['wc-ajax'] ) ) {
			wc_maybe_define_constant( 'DOING_AJAX', true );
			wc_maybe_define_constant( 'WC_DOING_AJAX', true );
			if ( ! WP_DEBUG || ( WP_DEBUG && ! WP_DEBUG_DISPLAY ) ) {
				@ini_set( 'display_errors', 0 ); // Turn off display_errors during AJAX events to prevent malformed JSON
			}
			$GLOBALS['wpdb']->hide_errors();
		}
	}

	/**
	 * Send headers for WC Ajax Requests.
	 *
	 * @since 2.5.0
	 */
	private static function wcp_ajax_headers() {
		send_origin_headers();
		@header( 'Content-Type: text/html; charset=' . get_option( 'blog_charset' ) );
		@header( 'X-Robots-Tag: noindex' );
		send_nosniff_header();
		nocache_headers();
		status_header( 200 );
	}

	/**
	 * Hook in methods - uses WordPress ajax handlers (admin-ajax).
	 */
	public static function add_ajax_events() {
		add_action( 'wp_ajax_woocommerce_printty2_' . 'order_create', array( __CLASS__, 'order_create') );
		add_action( 'wp_ajax_woocommerce_printty2_' . 'update_order_status', array( __CLASS__, 'update_order_status') );
		add_action( 'wp_ajax_woocommerce_printty2_' . 'fetch_order', array( __CLASS__, 'fetch_order') );
		add_action( 'wp_ajax_woocommerce_printty2_' . 'delete_order', array( __CLASS__, 'delete_order') );
		add_action( 'wp_ajax_woocommerce_printty2_' . 'save_order_item_image', array( __CLASS__, 'save_order_item_image') );
		add_action( 'wp_ajax_woocommerce_printty2_' . 'upload_order_invoice', array( __CLASS__, 'upload_order_invoice') );
		add_action( 'wp_ajax_woocommerce_printty2_' . 'delete_order_invoice', array( __CLASS__, 'delete_order_invoice') );
		add_action( 'wp_ajax_woocommerce_printty2_' . 'test', array( __CLASS__, 'test') );
		add_action( 'wp_ajax_woocommerce_printty2_' . 'mail_test', array( __CLASS__, 'mail_test') );
		add_action( 'wp_ajax_woocommerce_printty2_' . 'execute_task', array( __CLASS__, 'execute_task') );
		add_action( 'wp_ajax_nopriv_woocommerce_printty2_' . 'execute_task', array( __CLASS__, 'execute_task') );
		add_action( 'wp_ajax_woocommerce_printty2_' . 'callback', array( __CLASS__, 'callback') );
		add_action( 'wp_ajax_nopriv_woocommerce_printty2_' . 'callback', array( __CLASS__, 'callback') );
	}

	public static function save_order_item_image() {
		return WCP_Meta_Box_Order_Items::save();
	}

	/**
	 * Order to Printty2
	 */
	public static function order_create() {
		if ( ! current_user_can( 'edit_shop_orders' ) ) {
			wp_die( -1 );
		}

		$order = new WCP_Order(wc_get_order( absint( $_GET['order_id'] )));
		if ($order->order_to_printty2()) {
			wcp2i()->log('発注に成功しました！');
		}else{
			wcp2i()->log('発注に失敗しました。。。;;;');
			wcp2i()->log( $order->get_printty2_message() );
		}

		// ステータスを更新
		WCP_Order::update_orders_status_printty2();

		#wc_print_notices();
		wp_safe_redirect( wp_get_referer() ? wp_get_referer() : admin_url( 'edit.php?post_type=shop_order' ) );

		#wp_die();
	}

	/**
	 * Update Order Status by Printty2
	 */
	public static function update_order_status() {
		if ( ! current_user_can( 'edit_shop_orders' ) ) {
			wp_die( -1 );
		}
		WCP_Order::update_orders_status_printty2();
		wp_safe_redirect( wp_get_referer() ? wp_get_referer() : admin_url( 'edit.php?post_type=shop_order' ) );
	}

	/**
	 * Fetch Order from Printty2
	 */
	public static function fetch_order() {
		if ( ! current_user_can( 'edit_shop_orders' ) ) {
			wp_die( -1 );
		}
		$order = new WCP_Order(wc_get_order( absint( $_GET['order_id'] )));

		$response = $order->fetch_order_from_printty2();


		echo "<h2>Printty2から取得したStatus</h2>";
		echo "<pre>";
		print_r($response);
		echo "</pre>";
		echo "<hr>";

		echo "<h2>注文情報より生成した発注用のパラメータ</h2>";
		echo "<pre>";
		echo( WCP_Printty2API_Order_Parameter::build($order));
		echo "</pre>";
		echo "<hr>";

		echo "<h2>注文情報の入力チェック結果</h2>";
		echo "<pre>";
		echo(print_r($order->valid_for_order_to_printty2(), true));
		echo "</pre>";
		echo "<hr>";

		#wp_safe_redirect( wp_get_referer() ? wp_get_referer() : admin_url( 'edit.php?post_type=shop_order' ) );
	}

	// テストをキックする為のAPI
	public static function test() {
		if ( ! current_user_can( 'edit_shop_orders' ) ) {
			wp_die( -1 );
		}
		WCP_Printty2_Item::test();
		if( WCP_Install::is_installed()) {
			echo "installed";
		}
	}

	public static function mail_test() {
		if ( ! current_user_can( 'edit_shop_orders' ) ) {
			wp_die( -1 );
		}
		$order = wc_get_order( absint( $_GET['order_id'] ));

		WC()->mailer()->emails['WCP_Email_Customer_Processing_Order_Printty2']->trigger( $order->get_id(), $order );
		WC()->mailer()->emails['WCP_Email_Customer_Delivered_Order']->trigger( $order->get_id(), $order );
		WC()->mailer()->emails['WC_Email_Customer_On_Hold_Order']->trigger( $order->get_id(), $order );
		WC()->mailer()->emails['WC_Email_Customer_Processing_Order']->trigger( $order->get_id(), $order );
		WC()->mailer()->emails['WCP_Email_Customer_Request_Customers_Voice']->trigger( $order->get_id(), $order );

		wp_safe_redirect( wp_get_referer() ? wp_get_referer() : admin_url( 'edit.php?post_type=shop_order' ) );
	}

	/**
	 * Delete Order from Printty2
	 */
	public static function delete_order() {
		if ( ! current_user_can( 'edit_shop_orders' ) ) {
			wp_die( -1 );
		}
		$order = new WCP_Order(wc_get_order( absint( $_GET['order_id'] )));
		if ($order->delete_order_from_printty2()) {
			wcp2i()->log('注文の削除に成功しました。');
		}else{
			wcp2i()->log('注文の削除失敗しました。。。;;;' );
		}

		wp_safe_redirect( wp_get_referer() ? wp_get_referer() : admin_url( 'edit.php?post_type=shop_order' ) );
	}

	public static function upload_order_invoice(){
		if ( ! current_user_can( 'edit_shop_orders' ) ) {
			wp_die( -1 );
		}
		return WCP_Meta_Box_Order_Invoice::save();
	}

	public static function delete_order_invoice(){
		if ( ! current_user_can( 'edit_shop_orders' ) ) {
			wp_die( -1 );
		}
		$order = new WCP_Order(wc_get_order( absint( $_POST['order_id'] )));
		$order->delete_order_invoice();
	}

	public static function execute_task(){
		wcp2i()->log('start execute task-----------------------' );
		(new WCP_OrderTask)->update_ordered_orders_printty2_status();
		(new WCP_OrderTask)->send_mail_request_customers_voice();
		wcp2i()->log('end execute task----------------------------' );
	}

	public static function callback(){
		try {
			wcp2i()->log('execute callback--------------------------------' );
			wcp2i()->log( print_r(getallheaders(), true) );
			wcp2i()->log( print_r($_REQUEST, true) );

			$json_string = file_get_contents('php://input');

			$req = json_decode($json_string);
			wcp2i()->log( $json_string );
			wcp2i()->log( print_r($req, true) );

			foreach ( $req->orders as $key => $value ) {
				$printty2_order = $value->Order;
				$args = array(
					'printty2_id' => $printty2_order->id
				);
				$orders = wc_get_orders( $args );
				if ( count($orders) > 0 ){
					// 検索されたもの全て更新。※データの整合性的にはないはずだが、1個だけ更新したら逆にわかりずらかったため。
					foreach ( $orders as $o ) {
						wcp2i()->log("Order Delivered!! OrderId: " . $o->get_id());
						$order = new WCP_Order($o);
						# 配送番号の取得
						$order->fetch_order_from_printty2();
						$order->receive_sended_notification();
					}
				}else{
					wcp2i()->log("printty2より存在しないOrderIDが渡されました。ID : " . $printty2_order->id . '/' . $printty2_order->number . '/customer_number: ' . $printty2_order->customer_number . '/tracking_number: ' . $printty2_order->tracking_number);
				}
			}

			wcp2i()->log('end execute callback----------------------------' );
		} catch(Exception $e){
		}
	}
}

WCP_AJAX::init();
