<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WCP_Order {

	public function __construct(WC_Order $order){
		$this->order = $order;
		$this->meta_data = $order->get_meta_data();
	}

	/*
	 * Printty2へオーダーします
	 * @return boolean 成功/失敗
	 */
	public function order_to_printty2(){
		if ( $this->is_ordered_printty2() ){
			wcp2i()->log("already ordered. printty2_id: " . $printty2_id);
			return false;
		}
		$errors = $this->valid_for_order_to_printty2();
		if ( count($errors) != 0 ) {
			// パラメタが不足の場合エラーメッセージを設定
			array_unshift($errors, "入力内容が足りません。");
			$this->set_printty2_message(join("\n", $errors));
			$this->save();
			return false;
		}

		$param = WCP_Printty2API_Order_Parameter::build($this);
		$response = WCP_Printty2API_Orders::getInstance()->create($param);

		if ( !empty($response->message) ){
			$this->set_printty2_message('発注に失敗しました。: '. $response->message);
		}else{
			# 成功した場合はメッセージをクリア
			$this->set_printty2_message('');
		}

		if ( !empty($response->Order) ){
			$this->set_printty2_id($response->Order->id);
			$this->set_printty2_number($response->Order->number);
			$this->set_status('wc-processing-p2');
			$this->save();
		}else{
			$this->set_status('wc-failed-p2');
			$this->save();
			return false;
		}

		WC()->mailer()->emails['WCP_Email_Customer_Processing_Order_Printty2']->trigger( $this->get_id(), $this );

		return true;
	}

	/*
	 * Printty2への注文のステータスを取得します。
	 * 一つだけstatusを取得出来るAPIが無いようなので、全体を対象にする。
   *
	 * @return boolean 成功/失敗
	 */
	static public function update_orders_status_printty2(){
		$size = 1000;
		for($page=1;$page <= 40; $page++){
			$response = WCP_Printty2API_Orders::getInstance()->orders($page, $size);

			foreach ( $response->orders as $key => $value ) {
				$printty2_order = $value->Order;
				$args = array(
					'printty2_id' => $printty2_order->id
				);
				$orders = wc_get_orders( $args );
				if ( count($orders) > 0 ){
					// 検索されたもの全て更新。※データの整合性的にはないはずだが、1個だけ更新したら逆にわかりずらかったため。
					foreach ( $orders as $order ) {
						(new WCP_Order($order))->update_order_status_printty2($printty2_order);
					}
				}else{
					wcp2i()->log("printty2より存在しないOrderIDが渡されました。ID : " . $printty2_order->id . '/' . $printty2_order->number . '/' . $printty2_order->status_code);
				}
			}

			if ( intval($response->total_count) <= ($size * $page) ){
				wcp2i()->log(intval($response->total_count));
				return true;
			}
		}

		return true;
	}

	/**
	 * Printty2から取得したステータスをDBへ反映します。
	 */
	public function update_order_status_printty2($printty2_order){
		wcp2i()->log("update order status. OrderID:".$this->get_id()."/Printty2 ID:". $printty2_order->id . "/Status:" . $printty2_order->status_code);
		$this->set_printty2_number($printty2_order->number);
		$this->set_printty2_date($printty2_order->date);
		$this->set_printty2_production_date_preferred($printty2_order->production_date_preferred);
		$this->set_printty2_amount($printty2_order->amount);
		$this->set_printty2_status($printty2_order->status_code);

		// 画像取り込み中/取込エラーの場合、取り込まれたか確認
		if ($this->is_printty2_image_processing() || $this->is_printty2_image_processed_error() ){
			wcp2i()->log("Image Upload Check!!");
			$this->fetch_order_from_printty2();
		}

		// Printty2から配送済みのステータスが応答された場合。
		if(strcmp($this->get_status(), "sended-p2" ) != 0 
			&& strcmp($printty2_order->status_code, "shipping_complite") == 0){

			// tracking_numberを取得するため注文の情報を改めて取得
			$this->fetch_order_from_printty2();
			$this->receive_sended_notification();
		}

		$this->save();

		return true;
	}

	# 配送通知を受信時の処理
	public function receive_sended_notification(){
		// ステータスの更新と、配送済みのステータスに変更された日付を設定
		$this->set_printty2_sended_date(wcp2i()->get_date('Y-m-d'));
		$this->set_status('wc-sended-p2');
		$this->set_printty2_status('shipping_complite');

		// メールを送る前にデータを一度保存
		$this->save();
		WC()->mailer()->emails['WCP_Email_Customer_Delivered_Order']->trigger( $this->get_id(), $this );

		wcp2i()->log("Order Delivered!!");
	}

	public function fetch_order_from_printty2(){
		$response = WCP_Printty2API_Orders::getInstance()->fetch($this->get_printty2_id());
		$order = $response->Order;

		if( !empty($order->tracking_number) ){
			$this->set_printty2_tracking_number($order->tracking_number);
			wcp2i()->log("set tracking_number" . $order->tracking_number);
		}

		$errors = array();
		$image_processed = true;
		foreach ( $response->items as $item ) {
			foreach ( $item->tasks as $task ) {
				if($task->is_image_processing_error == 1){
					$errors[] = $item->Product->title . "は1画像のアップロードに失敗しています。";
				}
				if ($task->is_image_processed == 0){
					// 処理中のものが一つでもあれば、処理中とする。
					$image_processed = false;
				}
			}
		}
		if(!empty($errors) ){
			$this->set_printty2_message(join("\n", $errors));
			$this->set_printty2_image_processing_error();
			$this->set_status('wc-failed-p2');
		}else if($image_processed){
			$this->set_printty2_image_processed();
		}

		return $response;
	}

	/*
	 * Printty2へオーダーします
	 * @return boolean 成功/失敗
	 */
	public function delete_order_from_printty2(){
		if ( ! $this->is_ordered_printty2() ){
			wcp2i()->log("didn't ordered to printty2");
			return false;
		}
		$response = WCP_Printty2API_Orders::getInstance()->delete($this->get_printty2_id());
		if ( !empty($response->message) ){
			$this->set_printty2_message('注文に失敗しました。: '. $response->message);
		}else{
			$this->set_printty2_message('');
			$this->clear_printty2_data();
			$this->set_status('wc-processing-p2');
			$this->save();
		}

		$this->save();
	}

	/**
	 * Printty2からの注文情報を初期化します。
	 */
	private function clear_printty2_data(){
		$fields = array( '_printty2_id',
			'_printty2_number',
			'_printty2_date',
			'_printty2_production_date_preferred',
			'_printty2_status',
			'_printty2_amount',
			'_printty2_tracking_number',
			'_printty2_image_processed_status'
		);

		foreach ( $fields as $f ) {
			$this->delete_meta_data($f);
		}
		return true;
	}

	# 発注済か確認します。
	public function is_ordered_printty2(){
		$printty2_id = $this->get_printty2_id();
		if ( isset($printty2_id) && !empty($printty2_id)){
			return true;
		}
		return false;
	}

	# Getter / Setter
	public function set_printty2_id($id){
		$this->update_meta_data( '_printty2_id', intval($id) );
	}

	public function get_printty2_id(){
		return intval($this->get_meta( '_printty2_id'));
	}

	public function set_printty2_status($status){
		$this->update_meta_data( '_printty2_status', $status);
	}

	public function get_printty2_status(){
		return $this->get_meta( '_printty2_status');
	}

	public function get_printty2_status_name(){
		$status = $this->get_printty2_status();
		$staus_map = array(
			'created' => '受付済み',
			'production_processing' => '生産中',
			'shipping_complite' => '発送済み',
		);
		if( is_null($status) || empty($status) )
			return '';

		return $staus_map[$status];
	}

	public function set_order_type( $order_type ){
		$this->update_meta_data( '_order_type', $id );
	}

	public function get_order_type(){
		return $this->get_meta( '_order_type');
	}

	/**
	 * 配送時間
	 * 1: none(default)
	 * 2: 8:00-12:00
	 * 3: 12:00-16:00
	 * 4: 16:00-20:00
	 */
	public function get_shipping_delivery_period_id(){
		$id = $this->get_meta('_shipping_delivery_period_id');
		if ( is_null($id) || empty($id) )
			return 1;

		return intval($id);
	}

	/**
	 * 配送時間
	 */
	public function set_shipping_delivery_period_id($id){
		return $this->update_meta_data( '_shipping_delivery_period_id', intval($id));
	}

	/**
	 * ギフトセット
	 * 0 :not use(default)
	 * 1 :use
	 */
	public function get_use_package(){
		$use = $this->get_meta('_use_package');
		if ( is_null($use) || empty($use) )
			return 0;

		return intval($use);
	}

	/**
	 * ギフトセット
	 */
	public function set_use_package($id){
		return $this->update_meta_data( '_use_package', intval($id));
	}

	/**
	 * 出荷希望
	 * 3BusinessDay :3 business day (default)
	 * 5BusinessDay :5 business day
	 * 7BusinessDay :7 business day
	 * auto :auto
	 * 4BusinessDay :4 business day
	 * 9BusinessDay :9 business day
	 */
	public function get_priority_type_code(){
		$code = $this->get_meta('_priority_type_code');
		if ( is_null($code) || empty($code) )
			return '3BusinessDay';

		return $code;
	}

	/**
	 * 出荷希望
	 */
	public function set_priority_type_code($code){
		return $this->update_meta_data( '_priority_type_code', $code);
	}

	public function get_printty2_message(){
		return $this->get_meta('_priority_message');
	}

	/**
	 * 注文時のエラーメッセージ
	 */
	public function set_printty2_message($message){
		return $this->update_meta_data( '_priority_message', $message);
	}

	/**
	 * 注文時のエラーメッセージ
	 */
	public function get_product_items(){
		return array_filter($this->order->get_items(), function($elem){
			return strcmp(get_class($elem), 'WC_Order_Item_Product') == 0;
		});
	}

	# For Printty2 API

	# number
	public function get_printty2_number(){
		return $this->get_meta( '_printty2_number');
	}

	public function set_printty2_number($number){
		return $this->update_meta_data( '_printty2_number', $number);
	}

	# date
	public function get_printty2_date(){
		return $this->get_meta( '_printty2_date');
	}

	public function set_printty2_date($date){
		return $this->update_meta_data( '_printty2_date', $date);
	}

	# sended date
	public function get_printty2_sended_date(){
		return $this->get_meta( '_printty2_sended_date');
	}

	public function set_printty2_sended_date($date){
		return $this->update_meta_data( '_printty2_sended_date', $date);
	}

	# sended
	public function get_printty2_sended_request_customers_voice(){
		return $this->get_meta( '_printty2_sended_request_customers_voice');
	}

	public function set_printty2_sended_request_customers_voice(bool $sended){
		return $this->update_meta_data( '_printty2_sended_request_customers_voice', $sended);
	}

	# amount
	public function get_printty2_amount(){
		return $this->get_meta( '_printty2_amount');
	}

	public function set_printty2_amount($amount){
		return $this->update_meta_data( '_printty2_amount', $amount);
	}

	# production date preferred
	public function get_printty2_production_date_preferred(){
		return $this->get_meta( '_printty2_production_date_preferred');
	}

	public function set_printty2_production_date_preferred($date){
		return $this->update_meta_data( '_printty2_production_date_preferred', $date);
	}

	# customer number
	public function get_printty2_customer_number(){
		return $this->get_meta( '_printty2_customer_number');
	}

	public function set_printty2_customer_number($customer_number){
		return $this->update_meta_data( '_printty2_customer_number', $customer_number);
	}

	# tracking number
	public function get_printty2_tracking_number(){
		return $this->get_meta( '_printty2_tracking_number');
	}

	public function set_printty2_tracking_number($tracking_number){
		return $this->update_meta_data( '_printty2_tracking_number', $tracking_number);
	}

	# 配送元がSpicelifeか？
	public function is_delivery_source_spicelife(){
		return ( strcmp($this->get_delivery_source_company_for_printty2(), '株式会社spice life') === 0 );
	}

	# 配送元企業名
	public function get_delivery_source_company_for_printty2(){
		$company = $this->get_meta( '_delivery_source_company_for_printty2');

		if ( is_null($company) || empty($company) )
			return '株式会社spice life';

		return $company;
	}

	public function set_delivery_source_company_for_printty2($delivery_source_company){
		return $this->update_meta_data( '_delivery_source_company_for_printty2', $delivery_source_company);
	}

	# 配送元郵便番号
	public function get_delivery_source_postcode_for_printty2(){
		$postcode = $this->get_meta( '_delivery_source_postcode_for_printty2');

		if( $this->is_delivery_source_spicelife() )
			return '150-0002';

		return $postcode;
	}

	public function set_delivery_source_postcode_for_printty2($delivery_source_postcode){
		return $this->update_meta_data( '_delivery_source_postcode_for_printty2', $delivery_source_postcode);
	}

	# 配送元都道府県
	public function get_delivery_source_state_for_printty2(){
		$state = $this->get_meta( '_delivery_source_state_for_printty2');

		if( $this->is_delivery_source_spicelife() )
			return 'JP13';

		return $state;
	}

	public function get_delivery_source_state_name_for_printty2(){
		return WC()->countries->get_states('JP')[$this->get_delivery_source_state_for_printty2()];
	}

	public function set_delivery_source_state_for_printty2($delivery_source_state){
		return $this->update_meta_data( '_delivery_source_state_for_printty2', $delivery_source_state);
	}

	# 配送元市区町村
	public function get_delivery_source_city_for_printty2(){
		$city = $this->get_meta( '_delivery_source_city_for_printty2');

		if( $this->is_delivery_source_spicelife() )
			return '渋谷区';

		return $city;
	}

	public function set_delivery_source_city_for_printty2($delivery_source_city){
		return $this->update_meta_data( '_delivery_source_city_for_printty2', $delivery_source_city);
	}

	# 配送元住所1
	public function get_delivery_source_address1_for_printty2(){
		$address1 = $this->get_meta( '_delivery_source_address1_for_printty2');

		if( $this->is_delivery_source_spicelife() )
			return '渋谷3丁目28-8';

		return $address1;
	}

	public function set_delivery_source_address1_for_printty2($delivery_source_address1){
		return $this->update_meta_data( '_delivery_source_address1_for_printty2', $delivery_source_address1);
	}

	# 配送元住所2
	public function get_delivery_source_address2_for_printty2(){
		$address2 = $this->get_meta( '_delivery_source_address2_for_printty2');

		if( $this->is_delivery_source_spicelife() )
			return '第3久我屋ビル7F';

		return $address2;
	}

	public function set_delivery_source_address2_for_printty2($delivery_source_address2){
		return $this->update_meta_data( '_delivery_source_address2_for_printty2', $delivery_source_address2);
	}

	# 配送元電話番号
	public function get_delivery_source_phonenumber_for_printty2(){
		$phonenumber = $this->get_meta( '_delivery_source_phonenumber_for_printty2');

		if( $this->is_delivery_source_spicelife() )
			return '0364505416';

		return $phonenumber;
	}

	public function set_delivery_source_phonenumber_for_printty2($delivery_source_phonenumber){
		return $this->update_meta_data( '_delivery_source_phonenumber_for_printty2', $delivery_source_phonenumber);
	}

	/**
	 *	丸井さん側の画像の取り込みステータス: 取り込み完了
	 */
	public function is_printty2_image_processed(){
		$status = $this->get_meta( '_printty2_image_processed_status');
		return strcmp($status, 'processed') == 0;
	}

	/**
	 *	丸井さん側の画像の取り込みステータス: 処理中
	 */
	public function is_printty2_image_processing(){
		$status = $this->get_meta( '_printty2_image_processed_status');
		return is_null($status) || empty($status);
	}

	/**
	 *	丸井さん側の画像の取り込みステータス: 取り込みエラー
	 */
	public function is_printty2_image_processed_error(){
		$status = $this->get_meta( '_printty2_image_processed_status');
		return strcmp($status, 'error') == 0;
	}

	/**
	 *	丸井さん側の画像の取り込みステータス
	 */
	public function get_printty2_image_status(){
		if(!$this->is_ordered_printty2()) return null;
		$status = $this->get_meta( '_printty2_image_processed_status');
		if(is_null($status) || empty($status)) return 'processing';
		return $status;
	}

	/**
	 *	丸井さん側の画像の取り込みステータス: 取り込みエラーに設定
	 */
	public function set_printty2_image_processing_error(){
		return $this->update_meta_data( '_printty2_image_processed_status', 'error');
	}

	/**
	 *	丸井さん側の画像の取り込みステータス: 取り込み完了に設定
	 */
	public function set_printty2_image_processed(){
		return $this->update_meta_data( '_printty2_image_processed_status', 'processed');
	}


	public function get_billing_full_name(){
		return $this->get_billing_last_name() . ' ' . $this->get_billing_first_name();
	}

	public function get_shipping_full_name(){
		return $this->get_shipping_last_name() . ' ' . $this->get_shipping_first_name();
	}

	public function get_shipping_phone(){
		return $this->get_meta( '_shipping_phone');
	}

	public function get_billing_state_name(){
		$val = array( 'country' => $this->get_billing_country(), 'state' => $this->get_billing_state(), 'phone' => '');
		return str_replace('〒<br/>', '', WC()->countries->get_formatted_address( $val ) );
	}

	public function get_shipping_state_name(){
		$val = array( 'country' => $this->get_shipping_country(), 'state' => $this->get_shipping_state(), 'phone' => '');
		return str_replace('〒<br/>', '', WC()->countries->get_formatted_address( $val ) );
	}

	public function get_shipping_company_for_printty2(){
		return empty($this->get_shipping_company()) ?
			$this->get_shipping_full_name() : $this->get_shipping_company();
	}

	public function get_billing_company_for_printty2(){
		return empty($this->get_billing_company()) ?
			$this->get_billing_full_name() : $this->get_billing_company();
	}

	# Delivery Serivce Code
  # 配送会社の指定
	public function get_delivery_service_account_code(){
		return $this->get_delivery_service_account()['account_number'];
	}

	# Delivery Serivce Code
  # 配送会社の指定
	public function get_delivery_service_code(){
		return $this->get_delivery_service_account()['code'];
	}

	# Delivery Serivce Name
	# 配送会社の指定
	public function get_delivery_service_name(){
		return $this->get_delivery_service_account()['name'];
	}

	# Delivery Type Code
	# 配送形式の指定
	# 1: ネコポス[nekoposu]
	#	2: メール便
	#	3: 通常便[normalpost]
	#	4: 自動
	#	5: 代引き[korekuto]
	public function get_delivery_type_code(){
		$method = $this->get_delivery_method();
		if ( $method  == 1 ) {
			return 'korekuto';
		} else if ($method == 2 ){
			return 'nekoposu';
		} else {
			return 'normalpost';
		}
	}

	# Decide the delivery method.
	# @return [int]delivery method type.
	# 1. 決済方法が代引きの場合、
	# 　 佐川/代引き
	# 2.  注文個数が3個以下
	#     ヤマト/ ネコポス
	# 3. 上記以外
	#     佐川 / 通常便
	private function get_delivery_method() {
		if  ( strcmp ( $this->get_payment_method(), 'cod') == 0 ) {
			return 1;
		} else if ( $this->number_of_product_item() <= 3  ) {
			return 2;
		}else{
			return 3;
		}
	}

	# Delivery Serivce Code
  # 配送会社の指定
	private function get_delivery_service_account() {
		$method = $this->get_delivery_method();
		if ( $method  == 1 || $method == 3 ) {
			return array( 'code' => 'sagawa_spicelife', 'account_number' => "17761227-010", "name" => "佐川" );
		} else {
			return array( 'code' => 'yamato_spicelife', 'account_number' => "0332210313", "name" => "ヤマト" );
		}
	}

	# Number of Product Item.
	# @reutrn  [int] Number of Product Item.
	private function number_of_product_item() {
		$total_count = 0;
		if ( sizeof( $this->get_product_items() ) > 0 ) {
			foreach ( $this->get_product_items() as $item ) {
				$total_count += $item->get_quantity();
			}
		}

		return $total_count;
	}

	//valid before order to printty2.
	public function valid_for_order_to_printty2(){
		$errors = [];

		$require_fields = array(
			"delivery_source_company_for_printty2" => "配送元の企業名",
			"delivery_source_postcode_for_printty2"=> "配送元の郵便番号",
			"delivery_source_state_name_for_printty2"=> "配送元の都道府県",
			"delivery_source_address1_for_printty2"=> "配送元の住所詳細１",
			"delivery_service_account_code"=> "配送手段",
			"shipping_company_for_printty2"=> "配送先情報の姓名",
			"shipping_postcode"=> "配送先情報の郵便番号",
			"shipping_state_name"=> "配送先情報の都道府県",
			"shipping_address_1"=> "配送先情報の配送手段",
			"shipping_full_name"=> "配送先情報の姓名",
			"shipping_phone"=> "配送先情報の電話番号",
			"shipping_delivery_period_id"=> "配送時間",
			"use_package"=> "ギフトセット",
		);

		foreach( $require_fields as $field => $field_name ) {
			$method_name = 'get_' . $field;
			$value = strval($this->$method_name());
			if ($value == ''){
				$errors[] = $field_name . 'は必須です。';
			}
		}

		if(!$this->exists_invoice()){
			$errors[] = "請求書は必須です。";
		}

		# item's validate.
		if (count($this->get_product_items()) == 0 ){
			$errors[] = '商品は少なくとも一つは注文する必要があります。';
		}else{
			foreach ( $this->get_product_items() as $i) {
				$item = new WCP_Order_Item_Product($i);
				$errors = array_merge( $errors, $item->valid_for_order_to_printty2());
			}
		}

		return $errors;
	}

	# invoice
	public function set_invoice($file){
		$this->save_file('invoice.pdf', $file);
	}

	public function get_invoice_url($with_timestamp = false){
		$url = $this->get_upload_url() . '/invoice.pdf';
		if ($with_timestamp == true ) $url .= '?' . time();
		return $url;
	}

	public function get_invoice_path(){
		return $this->get_upload_dir() . '/invoice.pdf';
	}

	public function exists_invoice(){
		return is_readable($this->get_invoice_path());
	}

	public function delete_order_invoice(){
		return unlink($this->get_invoice_path());
	}

	// upload dir
	private function get_upload_dir(){
		return WP_CONTENT_DIR . '/uploads/printty2_upload/orders/' . $this->get_id();
	}

	private function get_upload_url(){
		return content_url() . '/uploads/printty2_upload/orders/' . $this->get_id();
	}

	/**
	 * アップロードされたファイルを保存します。
	 * 正しくはファイルを移動します。
	 * @param 保存するファイル名
	 * @param 保存するファイル
	 */
	private function save_file($name, $image){
		$dir = $this->get_upload_dir();
		$file = $dir . '/' . $name;

		if($image["tmp_name"]){
			list($file_name,$file_type) = explode(".", $image['name']);

			if(!file_exists($dir)){
				if(! mkdir($dir, 0755, true)){
					throw new RuntimeException("failed make dir : " . $dir);
				}
			}
			if (move_uploaded_file($image['tmp_name'], $file)) {
				chmod($file, 0644);
			}else{
				throw new RuntimeException("failed move file : " . $file);
			}
		}else{
			throw new RuntimeException("tmp_name not found!!!");
		}
		return true;
	}

	public function __call( $method, $parameters ) {
		if ( is_callable( array( $this->order, $method ) ) ) {
			$object = array_shift( $parameters );
			return call_user_func_array( array( $this->order, $method ), array_merge( array( &$object ), $parameters ) );
		}
	}
}

#WCP_Order::regist_action_woocommerce_before_order_object_save();
class WCP_Order_Filter {
	public function __construct(){
		add_filter( 'woocommerce_get_order_address', array( $this, 'filter_woocommerce_get_order_address' ));
	}

	public function filter_woocommerce_get_order_address($array){
		if(array_key_exists('first_name', $array)){
			$array['first_name'] = $array['first_name'] . ' 様';
		}
		return $array;
	}
}
new WCP_Order_Filter();

