<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>
<!--<p class="price">通常価格: --><?php //echo $product->get_price_html(); ?><!--</p>-->
<?php $down_price = wc_price( wc_get_price_to_display( $product, array('price' => $product->get_price() * 0.6 ) ) ) . $product->get_price_suffix(); ?>
<p class="down_price"><span class="label">業界最安</span> <?php echo $product->get_price_html(); ?></p>
<!--<span style="font-size: 12pt;"><strong><span style="color: #ff0000;">※カートに100個以上商品を追加頂くと自動適用されます。</span></strong></span>-->

<a href="https://drawer.orilab.jp/index.html?url_site=https://<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>&model_id=<?php echo $product->get_ID()?>">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dezasapo_btn.png" alt="デザイン相談・デザインサポート" />
</a>
