<?php
/**
 * Template Name: My account
 *
 * @subpackage Theme
 */

get_header();
?>

<script>
jQuery(function($){
    if(localStorage.getItem("UserLogin")!=null){
        $("#signin_signup"). hide();
    }
    else {
        $("#signin_signup"). show();
    }

    $("#login_btn").on('click', function () {
        var userName= $("#username").val();
        var passW= $("#password").val();

        $.post('https://api.orilab.jp/signin', {
            "email": userName,
            "password": passW
        }).done(function(serverResponse){
            if(serverResponse.session!=null && serverResponse.session!="" && serverResponse.session!=undefined){
                localStorage.setItem("UserLogin",serverResponse.session);
                localStorage.setItem("UserNickName",serverResponse.nickname);
                window.location.href =  window.location.origin;
            }
        }).fail(function(xhr, status, error) {
            alert('メール又はパスワードは正しくありません。ログインに失敗しました。');
        });
    });

    $("#singup_btn").on('click', function () {
        var nickname = $("#reg_nick_name").val();
        var fullname = $("#reg_full_name").val();
        var email= $("#reg_email").val();
        var password= $("#reg_password").val();

        $.post('https://api.orilab.jp/signup', {
            "nickname": nickname,
            "fullname": fullname,
            "email": email,
            "password": password
        }).done(function(serverResponse){
            if(serverResponse.session!=null && serverResponse.session!="" && serverResponse.session!=undefined){
                localStorage.setItem("UserLogin",serverResponse.session);
                localStorage.setItem("UserNickName",serverResponse.nickname);
                window.location.href =  window.location.origin;
            }
        }).fail(function(xhr, status, error) {
            alert('メールアドレスは既に存在します。');
            location.reload();
        });
    });
});

</script>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">


        <?php do_action( 'storefront_page_before' ); ?>
        <header class="entry-header">
            <h1 class="entry-title">マイアカウント</h1>
        </header>
                <div id="signin_signup" class="entry-content">
                    <div class="woocommerce">



                        <div class="u-columns col2-set" id="customer_login">

                            <div class="u-column1 col-1">


                                <h2>ログイン</h2>

                                <form class="woocommerce-form woocommerce-form-login login" method="post">


                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label for="username">ユーザー名またはメールアドレス <span class="required">*</span></label>
                                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="">
                                    </p>
                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label for="password">パスワード <span class="required">*</span></label>
                                        <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password">
                                    </p>


                                    <p class="form-row">
                                        <input type="hidden" id="woocommerce-login-nonce" name="woocommerce-login-nonce" value="56d86cf847"><input type="hidden" name="_wp_http_referer" value="/my-account">
                                        <input id="login_btn" type="button" class="woocommerce-Button button" name="login" value="ログイン">
                                        <!--                                <label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">-->
                                        <!--                                    <input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever"> <span>ログインしたままにする</span>-->
                                        <!--                                </label>-->
                                    </p>
                                    <p class="woocommerce-LostPassword lost_password">
                                        <a href="https://original-smaphocase.com/my-account/lost-password/">パスワードを忘れた場合はこちら</a>
                                    </p>


                                </form>


                            </div>

                            <div class="u-column2 col-2">

                                <h2>登録</h2>

                                <form method="post" class="register">



                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label for="reg_nickname">ニックネーム <span class="required">*</span></label>
                                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="nickName" id="reg_nick_name" value="">
                                    </p>

                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label for="reg_fullname">フルネーム <span class="required">*</span></label>
                                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="fullName" id="reg_full_name" value="">
                                    </p>

                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label for="reg_email">メールアドレス <span class="required">*</span></label>
                                        <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" value="">
                                    </p>


                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label for="reg_password">パスワード <span class="required">*</span></label>
                                        <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password">
                                    </p>



                                    <p class="woocommerce-FormRow form-row">
                                        <input type="hidden" id="woocommerce-register-nonce" name="woocommerce-register-nonce" value="7902981358"><input type="hidden" name="_wp_http_referer" value="/my-account">				<input id="singup_btn" type="button" class="woocommerce-Button button" name="register" value="登録">
                                    </p>


                                </form>

                            </div>

                        </div>

                    </div>					</div




        <?php do_action( 'storefront_page_after' );   ?>

    </main><!-- #main -->

</div><!-- #primary -->


<?php
do_action( 'storefront_sidebar' );
?>
<?php
get_footer();
?>




