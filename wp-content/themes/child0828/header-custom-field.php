<?php
if ( is_singular() ){//投稿・固定ページの場合
	if (!is_mobile()){
		$head_custom = get_post_meta($post->ID, 'header_custom', true);
		if ( $head_custom ) {
			echo $head_custom;
		}
	}else{
		$head_custom = get_post_meta($post->ID, 'header_custom_sp', true);
		if ( $head_custom ) {
			echo $head_custom;
		}
	}
}
