<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @class  WCP_Printty2_Image_Item_Size
 */
class WCP_Printty2_Image_Item_Size extends WCP_Printty2_Master_Base {
	public function __construct($product_id, $width, $height ){
		$this->product_id = $product_id;
		$this->width = $width;
		$this->height = $height;
	}

	static public function get_master_file_path(){
		return '/data/printty_item_image_sizes.csv';
	}

	static public function find_by_product_id($product_id) {
		$file = static::get_master_file();
		foreach ($file as $line) {
			if(!is_null($line[0]) && strcmp($line[0], $product_id) == 0){
				return new WCP_Printty2_Image_Item_Size($line[0], $line[1], $line[2]);
			}
		}

		return null;
	}

	public function get_id(){
		return null; 
	}

	public function get_title(){
		return null; 
	}

	public function get_width(){
		return $this->width; 
	}

	public function get_height(){
		return $this->height; 
	}

}
