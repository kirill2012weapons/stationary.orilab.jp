<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WCP_Install Class.
 */
class WCP_Install {

	/**
	 * Hook in tabs.
	 */
	public static function init() {
	}

	/**
	 * Install WCP.
	 */
	public static function install() {
		wp_schedule_event(time(), 'hourly', 'wcprintty2_update_order_status');
	}

	public static function uninstall() {
		wp_clear_scheduled_hook('wcprintty2_update_order_status');
	}

	public static function is_installed() {
		return wp_next_scheduled('wcprintty2_update_order_status');
	}
}
