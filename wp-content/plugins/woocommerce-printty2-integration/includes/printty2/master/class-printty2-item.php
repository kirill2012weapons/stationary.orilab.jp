<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @class  WCP_Printty2_Item 
 */
class WCP_Printty2_Item extends WCP_Printty2_Master_Base {
	public function __construct($id, $title, $category_id ){
		$this->id = $id;
		$this->title = $title;
		$this->category_id = $category_id;
	}

	static public function get_master_file_path(){
		return 'data/printty_items.csv';
	}

	public function get_item_colors(){
		return WCP_Printty2_Item_Color::find_by_product_id($this->get_id());
	}

	public function get_item_sizes(){
		return WCP_Printty2_Item_Size::find_by_product_id($this->get_id());
	}

	public function get_item_image_size(){
		return WCP_Printty2_Image_Item_Size::find_by_product_id($this->get_id());
	}

	static public function find_by_id ($id) {
		$file = static::get_master_file();

		foreach ($file as $line) {
			if(!is_null($line[0]) && strcmp($line[0], $id) == 0){
				return new WCP_Printty2_Item($line[0], $line[1], $line[2]);
			}
		}
		return null;
	}

	static public function all () {
		$file = static::get_master_file();
		foreach ($file as $line) {
			$records[] = new WCP_Printty2_Item($line[0], $line[1], $line[2]);
		}

		return $records;
	}

	// データの整合性チェック
	static public function test () {
		$items = static::all();

		foreach ($items as $item) {
			// スマホ以外はチェックしない。
			if ($item->category_id != 14) continue;
			$colors = $item->get_item_colors();
			if(empty($colors)) wcp2i()->log("マスタ不足(Color): " . print_r($item, true));
			foreach ($colors as $color) {
				$sides = $color->get_item_sides();
				if(empty($sides)) wcp2i()->log("マスタ不足(Side): " . print_r($color, true));
			}
			$sizes = $item->get_item_sizes();
			if(empty($sizes)) wcp2i()->log("マスタ不足(Size): " . print_r($item, true));
			$image_size = $item->get_item_image_size();
			if(is_null($image_size)) wcp2i()->log("マスタ不足(ImageSize): " . print_r($item, true));
		}
		return true;
	}


}
